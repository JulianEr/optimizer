using Pkg

cd("$(@__DIR__)/../")

Pkg.activate(".")

using PackageCompiler

create_app(".", "./build")