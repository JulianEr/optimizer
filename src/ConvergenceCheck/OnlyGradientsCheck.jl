struct OnlyGradientsCheck <: AbstractConvergenceCheck
    gradientRMSThreshold::Float64
    gradientMAXThreshold::Float64
end

OnlyGradientsCheck(gradientMAXThreshold::Float64 = 2.5e-3) = OnlyGradientsCheck(gradientMAXThreshold, 1.7e-3)

function isConvergenceReached(self::OnlyGradientsCheck, optimizer::AbstractOptimizer)
    gMax, gRMS = let g = getActiveGradients(optimizer)
        (maximum(abs.(g)), sqrt(1.0/length(g)*dot(g,g)))
    end

    @info(@sprintf(
"Checking For Convergence!
Convergence Criteria: MAX(g) = %10.5e RMS(g) = %10.5e
Values this step:     MAX(g) = %10.5e RMS(g) = %10.5e", 
    self.gradientMAXThreshold, self.gradientRMSThreshold, gMax, gRMS))

    if gRMS > self.gradientRMSThreshold || abs(gMax) > self.gradientMAXThreshold
        return false
    else
        return true
    end
end