function julia_main()::Cint
    println("Starting Tropic!")
    arguments = parseArguments()
    @time begin
        optimize(arguments)
    end
    return 0
end