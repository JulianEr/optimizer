struct BondConstraint <: Constraint
    a::Int64
    b::Int64
end

function ==(a::Bond, b::BondConstraint)
    a.a == b.a && a.b == b.b || a.b == b.a && a.a == b.b
end

function ==(a::BondConstraint, b::Bond)
    b == a
end

function ==(a::HydrogenBond, b::BondConstraint)
    a.bond.a == b.a && a.bond.b == b.b || a.bond.b == b.a && a.bond.a == b.b
end

function ==(a::BondConstraint, b::HydrogenBond)
    b == a
end

function reorderBondConstraint(b::BondConstraint, newOrder::Array{Int})
    BondConstraint(newOrder[b.a], newOrder[b.b])
end