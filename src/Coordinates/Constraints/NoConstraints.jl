struct NoConstraints <: SetOfConstraints end

hasConstraints(::NoConstraints) = false

function addTranslation!(::NoConstraints, translationX::TranslationXConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], PrimitiveConstraints([], [], []))
    addTranslation!(newConstraintSet, translationX)
end

function addTranslation!(::NoConstraints, translationY::TranslationYConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], PrimitiveConstraints([], [], []))
    addTranslation!(newConstraintSet, translationY)
end

function addTranslation!(::NoConstraints, translationZ::TranslationZConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], PrimitiveConstraints([], [], []))
    addTranslation!(newConstraintSet, translationZ)
end

function addRotation!(::NoConstraints, rotationA::RotationAConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], PrimitiveConstraints([], [], []))
    addRotation!(newConstraintSet, rotationA)
end

function addRotation!(::NoConstraints, rotationB::RotationBConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], PrimitiveConstraints([], [], []))
    addRotation!(newConstraintSet, rotationB)
end

function addRotation!(::NoConstraints, rotationC::RotationCConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], PrimitiveConstraints([], [], []))
    addRotation!(newConstraintSet, rotationC)
end

reorderConstraints(::NoConstraints, ::Array{Int}) = NoConstraints()