struct Angle <: AbstractInternalCoordinate
    a::Int
    b::Int
    c::Int
end

struct AngleIsLinearException <:Exception
    msg::String
end

function ==(a::Angle, b::Angle)
    a.a == b.a && a.b == b.b && a.c == b.c || a.a == b.c && a.b == b.b && a.c == b.a
end

function value(self::Angle, xs::Array{Float64})
    a = getCartesian(self.a, xs)
    b = getCartesian(self.b, xs)
    c = getCartesian(self.c, xs)

    u = a-b
    v = c-b

    uv = dot(u,v)/norm(u)/norm(v)

    acos(clamp(uv, -1, 1))
end

const ortho1 = [1.0; -1.0; 1.0]/norm([1.0; -1.0; 1.0])
const ortho2 = [-1.0; 1.0; 1.0]/norm([-1.0; 1.0; 1.0])

function derivative(a::Array{Float64}, b::Array{Float64}, c::Array{Float64})
    u = a-b
    v = c-b
    λ_u = norm(u)
    λ_v = norm(v)
    u /= λ_u
    v /= λ_v
    
    ɛ = 0.01
    w = Array{Float64}(undef, 3)
    if abs(dot(u, v)) > (1.0 - ɛ)
        if abs(dot(u, ortho1)) > (1.0 - ɛ)
            w = cross(u, ortho2)
        else
            w = cross(u, ortho1)
        end
    else
        w = cross(u,v)
    end

    w /= norm(w);
	der1 = cross(u,w) / λ_u;
	der2 = cross(w,v) / λ_v;
	#                       a       b           c
	#                       m       o           n
	(der1, -der1 - der2, der2);
end

function derivativeVector(self::Angle, xs::Array{Float64})
    vec = fill(0.0, length(xs))

    a = getCartesian(self.a, xs)
    b = getCartesian(self.b, xs)
    c = getCartesian(self.c, xs)

    vec[(self.a-1)*3+1:(self.a-1)*3+3], 
    vec[(self.b-1)*3+1:(self.b-1)*3+3],
    vec[(self.c-1)*3+1:(self.c-1)*3+3] = derivative(a, b, c)

    vec
end

function getTerms(a::Array{Float64}, b::Array{Float64}, c::Array{Float64})
    u = a-b
    v = c-b

    λu = norm(u)
    λv = norm(v)

    u = u/λu
    v = v/λv

    ɛ = 0.01
    if abs(dot(u,v)) > (1.0 - ɛ) 
        throw(AngleIsLinearException("Angle is linear second derivatives are undefined."))
    end

    uv = u*v'
    uu = u*u'
    vv = v*v'

    cosq = dot(u,v)
    sinq = sqrt(1.0 - cosq^2.0)

    (term1(uv, uu, cosq, sinq, λu), term2(uv, vv, cosq, sinq, λv), 
     term3(uu, vv, uv, cosq, sinq, λu, λv), term4(vv, uu, uv, cosq, sinq, λu, λv), cosq/sinq)
end

function term1(uv::Array{Float64, 2}, uu::Array{Float64, 2}, cosq::Float64, sinq::Float64, λu::Float64)
    t1 = uv + transpose(uv) - 3.0*uu*cosq

    t1[1,1] += cosq
    t1[2,2] += cosq
    t1[3,3] += cosq

    t1 ./= (λu^2.0*sinq)
    t1
end

function term2(uv::Array{Float64, 2}, vv::Array{Float64, 2}, cosq::Float64, sinq::Float64, λv::Float64)
    t2 = uv + transpose(uv) - 3.0*vv*cosq

    t2[1,1] += cosq
    t2[2,2] += cosq
    t2[3,3] += cosq

    t2 ./= (λv^2.0*sinq)
    t2
end

function term3(uu::Array{Float64, 2}, vv::Array{Float64, 2}, uv::Array{Float64, 2}, cosq::Float64, sinq::Float64, λu::Float64, λv::Float64)
    t3 = uu + vv - uv*cosq

    t3[1,1] += -1.0
    t3[2,2] += -1.0
    t3[3,3] += -1.0

    t3 ./= (λu*λv*sinq)
    t3
end

function term4(vv::Array{Float64, 2}, uu::Array{Float64, 2}, uv::Array{Float64, 2}, cosq::Float64, sinq::Float64, λu::Float64, λv::Float64)
    t4 = vv + uu - transpose(uv)*cosq

    t4[1,1] += -1.0
    t4[2,2] += -1.0
    t4[3,3] += -1.0

    t4 ./= (λu*λv*sinq)
    t4
end

function secondDerivative(self::Angle, xs::Array{Float64})
    a = getCartesian(self.a, xs)
    b = getCartesian(self.b, xs)
    c = getCartesian(self.c, xs)

    t1, t2, t3, t4, cossin = getTerms(a, b, c)
    ∂a, ∂b, ∂c = derivative(a,b,c)

    aa = t1                - cossin * (∂a*∂a')
    ab = -t1 - t3          - cossin * (∂a*∂b')
    ac = t3                - cossin * (∂a*∂c')
    ba = -t1 -t4           - cossin * (∂b*∂a')
    bb = t1 + t2 + t3 + t4 - cossin * (∂b*∂b')
    bc = -t2 - t3          - cossin * (∂b*∂c')
    ca = t4                - cossin * (∂c*∂a')
    cb = -t2 - t4          - cossin * (∂c*∂b')
    cc = t2                - cossin * (∂c*∂c')

    aa, ab, ac, ba, bb, bc, ca, cb, cc
end

function secondDerivativeMatrix(self::Angle, xs::Array{Float64})
    mat = fill(0.0, (length(xs), length(xs)))

    try
        mat[(self.a-1)*3+1:(self.a-1)*3+3, (self.a-1)*3+1:(self.a-1)*3+3],
        mat[(self.a-1)*3+1:(self.a-1)*3+3, (self.b-1)*3+1:(self.b-1)*3+3],
        mat[(self.a-1)*3+1:(self.a-1)*3+3, (self.c-1)*3+1:(self.c-1)*3+3],
        mat[(self.b-1)*3+1:(self.b-1)*3+3, (self.a-1)*3+1:(self.a-1)*3+3],
        mat[(self.b-1)*3+1:(self.b-1)*3+3, (self.b-1)*3+1:(self.b-1)*3+3],
        mat[(self.b-1)*3+1:(self.b-1)*3+3, (self.c-1)*3+1:(self.c-1)*3+3],
        mat[(self.c-1)*3+1:(self.c-1)*3+3, (self.a-1)*3+1:(self.a-1)*3+3],
        mat[(self.c-1)*3+1:(self.c-1)*3+3, (self.b-1)*3+1:(self.b-1)*3+3],
        mat[(self.c-1)*3+1:(self.c-1)*3+3, (self.c-1)*3+1:(self.c-1)*3+3] = secondDerivative(self, xs)
    catch _
        @warn(@sprintf("Angle %d %d %d is linear. Using zeroes as second derivatives", self.a, self.b, self.c))
    end
    mat
end

const firstPeriodConstantSchlegel = 0.160
const higehrPeriodConstantSchlegel = 0.250

angleForceConstantSchlegel(::FirstPeriod, ::Period, ::Period) = firstPeriodConstantSchlegel
angleForceConstantSchlegel(::Period, ::FirstPeriod, ::Period) = firstPeriodConstantSchlegel
angleForceConstantSchlegel(::Period, ::Period, ::FirstPeriod) = firstPeriodConstantSchlegel
angleForceConstantSchlegel(::FirstPeriod, ::FirstPeriod, ::Period) = firstPeriodConstantSchlegel
angleForceConstantSchlegel(::FirstPeriod, ::Period, ::FirstPeriod) = firstPeriodConstantSchlegel
angleForceConstantSchlegel(::Period, ::FirstPeriod, ::FirstPeriod) = firstPeriodConstantSchlegel
angleForceConstantSchlegel(::FirstPeriod, ::FirstPeriod, ::FirstPeriod) = firstPeriodConstantSchlegel
angleForceConstantSchlegel(::Period, ::Period, ::Period) = higehrPeriodConstantSchlegel

function angleForceConstantSchlegel(left::String, middle::String, right::String, ::Float64, ::Float64)
    l, m, r = placeInPeriodicTable[left], placeInPeriodicTable[middle], placeInPeriodicTable[right]
    
    angleForceConstantSchlegel(l, m, r)
end

function getHessianGuessSchlegel(a::Angle, symbols::Array{String}, cartesians::Array{Float64})
    angleForceConstantSchlegel(symbols[a.a], symbols[a.b], symbols[a.c], getDistanceBetweenCartesianPointAandB(a.a, a.b, cartesians), getDistanceBetweenCartesianPointAandB(a.b, a.c, cartesians))
end

getHessianGuessLindh(::Angle, ::HigherPeriod, ::Period, ::Period, ::Array{Float64}) = 0.2
getHessianGuessLindh(::Angle, ::Period, ::HigherPeriod, ::Period, ::Array{Float64}) = 0.2
getHessianGuessLindh(::Angle, ::Period, ::Period, ::HigherPeriod, ::Array{Float64}) = 0.2
getHessianGuessLindh(::Angle, ::Period, ::HigherPeriod, ::HigherPeriod, ::Array{Float64}) = 0.2
getHessianGuessLindh(::Angle, ::HigherPeriod, ::Period, ::HigherPeriod, ::Array{Float64}) = 0.2
getHessianGuessLindh(::Angle, ::HigherPeriod, ::HigherPeriod, ::Period, ::Array{Float64}) = 0.2
getHessianGuessLindh(::Angle, ::HigherPeriod, ::HigherPeriod, ::HigherPeriod, ::Array{Float64}) = 0.2

function getHessianGuessLindh(a::Angle, l::Period, m::Period, r::Period, cartesians::Array{Float64})
    leftBondLength = getDistanceBetweenCartesianPointAandB(a.a, a.b, cartesians)
    rightBondLength = getDistanceBetweenCartesianPointAandB(a.b, a.c, cartesians)
    0.15*simpleForceConstantsρ(l, m, leftBondLength)*simpleForceConstantsρ(m, r, rightBondLength)
end

function getHessianGuessLindh(a::Angle, symbols::Array{String}, cartesians::Array{Float64})
    left, middle, right = symbols[a.a], symbols[a.b], symbols[a.c]
    l, m, r = placeInPeriodicTable[left], placeInPeriodicTable[middle], placeInPeriodicTable[right]
    getHessianGuessLindh(a, l, m, r, cartesians)
end

toString(a::Angle) = "Angle($(a.a), $(a.b), $(a.c))"