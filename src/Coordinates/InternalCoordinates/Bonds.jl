struct Bond <: AbstractInternalCoordinate
    a::Int
    b::Int
end

function ==(a::Bond, b::Bond)
    a.a == b.a && a.b == b.b || a.b == b.a && a.a == b.b
end

getA(self::Bond) = self.a
getB(self::Bond) = self.b

value(self::Bond, xs::Array{Float64}) = getDistanceBetweenCartesianPointAandB(self.a, self.b, xs)

function derivative(self::Bond, xs::Array{Float64})
    a = getCartesian(self.a, xs)
    b = getCartesian(self.b, xs)

    u = a-b
    u /= norm(u)

    (u, -u)
end

function derivativeVector(self::Bond, xs::Array{Float64})
    vec = fill(0.0, length(xs))

    vec[(self.a-1)*3+1:(self.a-1)*3+3], vec[(self.b-1)*3+1:(self.b-1)*3+3] = derivative(self, xs)

    vec
end

function secondDerivative(self::Bond, xs::Array{Float64})
    a = getCartesian(self.a, xs)
    b = getCartesian(self.b, xs)

    u = (b-a)
    λ_u = norm(u) 
    u /= λ_u

    aa = u*u'
    aa[1,1] = aa[1,1] - 1.0 
    aa[2,2] = aa[2,2] - 1.0
    aa[3,3] = aa[3,3] - 1.0
    aa./= λ_u
    
    ab = aa
    ba = aa
    bb = -1.0 * aa
    aa *= -1.0

    aa, ab, ba, bb
end


function secondDerivativeMatrix(self::Bond, xs::Array{Float64})
    mat = fill(0.0, (length(xs), length(xs)))

    mat[(self.a-1)*3+1:(self.a-1)*3+3, (self.a-1)*3+1:(self.a-1)*3+3],
    mat[(self.a-1)*3+1:(self.a-1)*3+3, (self.b-1)*3+1:(self.b-1)*3+3],
    mat[(self.b-1)*3+1:(self.b-1)*3+3, (self.a-1)*3+1:(self.a-1)*3+3],
    mat[(self.b-1)*3+1:(self.b-1)*3+3, (self.b-1)*3+1:(self.b-1)*3+3] = secondDerivative(self, xs)

    mat
end

bondForceConstantSchlegel(::FirstPeriod, ::FirstPeriod) = -0.244
bondForceConstantSchlegel(::FirstPeriod, ::SecondPeriod) = 0.352
bondForceConstantSchlegel(a::SecondPeriod, b::FirstPeriod) = bondForceConstantSchlegel(b, a)
bondForceConstantSchlegel(::FirstPeriod, ::ThirdPeriod) = 0.660
bondForceConstantSchlegel(::FirstPeriod, ::HigherPeriod) = 0.660
bondForceConstantSchlegel(a::ThirdPeriod, b::FirstPeriod) = bondForceConstantSchlegel(b, a)
bondForceConstantSchlegel(a::HigherPeriod, b::FirstPeriod) = bondForceConstantSchlegel(b, a)
bondForceConstantSchlegel(::SecondPeriod, ::SecondPeriod) = 1.085
bondForceConstantSchlegel(::SecondPeriod, ::ThirdPeriod) = 1.522
bondForceConstantSchlegel(::SecondPeriod, ::HigherPeriod) = 1.522
bondForceConstantSchlegel(a::ThirdPeriod, b::SecondPeriod) = bondForceConstantSchlegel(b, a)
bondForceConstantSchlegel(a::HigherPeriod, b::SecondPeriod) = bondForceConstantSchlegel(b, a)
bondForceConstantSchlegel(::ThirdPeriod, ::ThirdPeriod) = bondForceConstantSchlegel(HigherPeriod(), HigherPeriod())
bondForceConstantSchlegel(::ThirdPeriod, ::HigherPeriod) = bondForceConstantSchlegel(HigherPeriod(), HigherPeriod())
bondForceConstantSchlegel(::HigherPeriod, ::ThirdPeriod) = bondForceConstantSchlegel(HigherPeriod(), HigherPeriod())
bondForceConstantSchlegel(::HigherPeriod, ::HigherPeriod) = 2.068

function bondForceConstantSchlegel(left::String, right::String, bondLength::Float64)
    l, r = placeInPeriodicTable[left], placeInPeriodicTable[right]
    B = bondForceConstantSchlegel(l, r)
    1.734 / ((bondLength-B)^3)
end

function getHessianGuessSchlegel(b::Bond, symbols::Array{String}, cartesians::Array{Float64})
    bondForceConstantSchlegel(symbols[b.a], symbols[b.b], value(b, cartesians))
end

getHessianGuessLindh(::Bond, ::HigherPeriod, ::Period, ::Array{Float64}) = 0.5
getHessianGuessLindh(::Bond, ::Period, ::HigherPeriod, ::Array{Float64}) = 0.5
getHessianGuessLindh(::Bond, ::HigherPeriod, ::HigherPeriod, ::Array{Float64}) = 0.5

function getHessianGuessLindh(b::Bond, l::Period, r::Period, cartesians::Array{Float64})
    bondLength = value(b, cartesians)
    0.45 * simpleForceConstantsρ(l, r, bondLength)
end

function getHessianGuessLindh(b::Bond, symbols::Array{String}, cartesians::Array{Float64})
    left, right = symbols[b.a], symbols[b.b]
    l, r = placeInPeriodicTable[left], placeInPeriodicTable[right]

    getHessianGuessLindh(b, l, r, cartesians)
end

toString(b::Bond) = "Bond($(b.a), $(b.b))"