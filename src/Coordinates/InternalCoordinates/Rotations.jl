## TODO Try and remove Radius of gyration and see what happens

struct Rotation 
    indices::Array{Int}
    reference::Array{Float64, 2}
    radiusOfGyration::Float64
end


function getRadiusIfGyration(reference::Array{Float64, 2})
    norm(reference)/sqrt(size(reference, 1))
end

function getCartesiansForIndicesInBaryCenter(indices::Array{Int}, cartesians::Array{Float64})
    ref = Array{Float64}(undef, length(indices), 3)
    for i ∈ 1:length(indices)
        ref[i, :] = cartesians[indices[i]*3-2:indices[i]*3]
    end
    toBaryCenter(ref)
end

function Rotation(indices::Array{Int}, cartesians::Array{Float64})
    reference = getCartesiansForIndicesInBaryCenter(indices, cartesians)
    Rotation(indices, reference, getRadiusIfGyration(reference))
end

function RotationForSlicedCenteredCartesian(indices::Array{Int}, cartesians::Array{Float64, 2})
    Rotation(indices, cartesians, getRadiusIfGyration(cartesians))
end

RotationAForSlicedCenteredCartesian(indices::Array{Int}, cartesians::Array{Float64, 2}) = RotationA(RotationForSlicedCenteredCartesian(indices, cartesians))

RotationBForSlicedCenteredCartesian(indices::Array{Int}, cartesians::Array{Float64, 2}) = RotationB(RotationForSlicedCenteredCartesian(indices, cartesians))

RotationCForSlicedCenteredCartesian(indices::Array{Int}, cartesians::Array{Float64, 2}) = RotationC(RotationForSlicedCenteredCartesian(indices, cartesians))

struct RotationA <: AbstractInternalCoordinate
    rotation::Rotation
end

struct RotationB <: AbstractInternalCoordinate
    rotation::Rotation
end

struct RotationC <: AbstractInternalCoordinate
    rotation::Rotation
end

RotationA(indices::Array{Int}, cartesians::Array{Float64}) = RotationA(Rotation(indices, cartesians))
RotationB(indices::Array{Int}, cartesians::Array{Float64}) = RotationB(Rotation(indices, cartesians))
RotationC(indices::Array{Int}, cartesians::Array{Float64}) = RotationC(Rotation(indices, cartesians))

function ==(a::RotationA, b::RotationA)
    a.rotation.indices == b.rotation.indices
end

function ==(a::RotationB, b::RotationB)
    a.rotation.indices == b.rotation.indices
end

function ==(a::RotationC, b::RotationC)
    a.rotation.indices == b.rotation.indices
end

value(self::RotationA, xs::Array{Float64}) = value(self.rotation, 1, xs)

value(self::RotationB, xs::Array{Float64}) = value(self.rotation, 2, xs)

value(self::RotationC, xs::Array{Float64}) = value(self.rotation, 3, xs)

function value(self::Rotation, direction::Int, xs::Array{Float64})
    newStructure = getCartesiansForIndicesInBaryCenter(self.indices, xs)
    
    _, q, _ = getBestFittingQuaternionAndF(self.reference, newStructure)
    
    p = let qm1 = q[1] - 1 
        if abs(qm1) < 1.e-4
            2 - 2*qm1/3
        else
            2*(acos(q[1])/sqrt(1 - q[1]^2))
        end
    end
    q[direction+1]*p*self.radiusOfGyration
end

function dR(i::Int, j::Int, k::Int, n::Int, oldCartesians::Array{Float64, 2})
    if i != n
        0.0
    else
        oldCartesians[k, j]
    end
end

function dF(k::Int, n::Int, oldCartesians::Array{Float64, 2})
    r = (i, j) -> dR(i, j, k, n, oldCartesians)
    [(r(1,1) + r(2,2) + r(3,3)) (r(2,3) - r(3,2))          ( r(3,1) - r(1,3))          ( r(1,2) - r(2,1));
     (r(2,3) - r(3,2))          (r(1,1) - r(2,2) - r(3,3)) ( r(1,2) + r(2,1))          ( r(1,3) + r(3,1));
     (r(3,1) - r(1,3))          (r(1,2) + r(2,1))          (-r(1,1) + r(2,2) - r(3,3)) ( r(2,3) + r(3,2));
     (r(1,2) - r(2,1))          (r(1,3) + r(3,1))          ( r(2,3) + r(3,2))          (-r(1,1) - r(2,2) + r(3,3))]
end

function derValueDerQuaternion(q::Array{Float64}, direction::Int)
    res = fill(0.0, 4)
    if abs(q[1] - 1) < 1.e-4
        res[1] = -2/3 * q[direction+1]
        res[direction+1] = 2 - 2*(q[1] - 1)/3
    else 
        res[1] = 2*q[direction+1] * ((q[1]*acos(q[1]))/((1 - q[1]^2)^(3/2)) - 1/(1 - q[1]^2))
        res[direction+1] = 2*(acos(q[1])/sqrt(1 - q[1]^2))
    end
    res
end

function derivative(self::Rotation, direction::Int, xs::Array{Float64})
    newStructure = getCartesiansForIndicesInBaryCenter(self.indices, xs)
    
    λ, q, F = getBestFittingQuaternionAndF(self.reference, newStructure)
    Θ⁻¹ = pinv(F - λ*I + λ*q*q')
    ∂v∂q = derValueDerQuaternion(q, direction)
    ∂v∂x = Array{Float64}(undef, length(self.indices), 3)

    for i ∈ 1:size(∂v∂x, 1)
        for j ∈ 1:size(∂v∂x, 2)
            dF_ij = dF(i, j, self.reference)
            Π = q'*dF_ij*q*I - dF_ij
            dq = Θ⁻¹*Π*q
            ∂v∂x[i, j] = ∂v∂q' * dq
        end
    end

    ∂v∂x
end

derivativeVector(self::RotationA, xs::Array{Float64}) = derivativeVector(self.rotation, 1, xs)

derivativeVector(self::RotationB, xs::Array{Float64}) = derivativeVector(self.rotation, 2, xs)

derivativeVector(self::RotationC, xs::Array{Float64}) = derivativeVector(self.rotation, 3, xs)

function derivativeVector(self::Rotation, direction::Int, xs::Array{Float64})
    vec = fill(0.0, length(xs))
    der = derivative(self, direction, xs)

    for i ∈ 1:length(self.indices)
        vec[self.indices[i]*3-2:self.indices[i]*3] = der[i, :]
    end

    vec*self.radiusOfGyration
end

secondDerivativeMatrix(self::RotationA, xs::Array{Float64}) = secondDerivativeMatrix(self.rotation, 1, xs)

secondDerivativeMatrix(self::RotationB, xs::Array{Float64}) = secondDerivativeMatrix(self.rotation, 2, xs)

secondDerivativeMatrix(self::RotationC, xs::Array{Float64}) = secondDerivativeMatrix(self.rotation, 3, xs)

function secondDerValueDerQuat(q::Array{Float64}, direction::Int)
    res = fill(0.0, 4, 4)
    if abs(q[1] - 1) < 1.e-4
        res[1, 1] = 0
        res[1, direction+1] = -2/3
        res[direction+1, 1] = -2/3
    else 
        acosq0 = acos(q[1])
        one_m_q2 = 1 - q[1]^2
        q2_m_one = q[1]^2 - 1
        res[1, 1] = 2*q[direction+1] * ((acosq0*(2*q[1]^2 + 1))/(one_m_q2)^(5/2) - 3*q[1]/(q2_m_one)^2)
        res[1, direction+1] = 2*(q[1]*acosq0/(one_m_q2)^(3/2) + 1/(q2_m_one))
        res[direction+1, 1] = res[1, direction+1]
    end
    res
end

function secondDerivative(self::Rotation, direction::Int, xs::Array{Float64})
    newStructure = getCartesiansForIndicesInBaryCenter(self.indices, xs)
    
    λ, q, F = getBestFittingQuaternionAndF(self.reference, newStructure)
    Θ⁻¹ = pinv(F - λ*I + λ*q*q')

    ∂v∂q = derValueDerQuaternion(q, direction)
    ∂∂v∂∂q = secondDerValueDerQuat(q, direction)

    ∂∂v∂∂x = Array{Float64}(undef, length(self.indices), length(self.indices), 3, 3)

    for k ∈ 1:size(∂∂v∂∂x, 1)
        for i ∈ 1:size(∂∂v∂∂x, 3)
            dF_ki = dF(k, i, self.reference)
            Π_ki = q'*dF_ki*q*I - dF_ki
            dq_ki = Θ⁻¹*Π_ki*q
            for l ∈ 1:size(∂∂v∂∂x, 2)
                for j ∈ 1:size(∂∂v∂∂x, 4)
                    dF_lj = dF(l, j, self.reference)
                    Π_lj = q'*dF_lj*q*I - dF_lj
                    dq_lj = Θ⁻¹*Π_lj*q
                    ∂∂λ = q'*(dF_ki - q'*dF_ki*q*I)*dq_lj + q'*(dF_lj - q'*dF_lj*q*I)*dq_ki
                    ∂Θ = dF_lj - q'*dF_lj*q*I + q'*dF_lj*q*q*q' + λ*(dq_lj*q' + q*dq_lj')
                    ddq = Θ⁻¹*(∂∂λ*q + Π_ki*dq_lj - ∂Θ*dq_ki)
                    ∂∂v∂∂x[k, l, i, j] = sum(∂v∂q'*ddq) + dq_lj'*∂∂v∂∂q*dq_ki
                end
            end
        end
    end

    ∂∂v∂∂x
end

function secondDerivativeMatrix(self::Rotation, direction::Int, xs::Array{Float64})
    mat = fill(0.0, (length(xs), length(xs)))
    ders = secondDerivative(self, direction, xs)

    for i ∈ 1:length(self.indices) 
        for j ∈ 1:length(self.indices)
            mat[self.indices[i]*3-2:self.indices[i]*3, self.indices[j]*3-2:self.indices[j]*3] = ders[i, j, : , :]
        end
    end

    mat*self.radiusOfGyration
end

getHessianGuessSchlegel(::RotationA, ::Array{String}, ::Array{Float64}) = 0.05
getHessianGuessSchlegel(::RotationB, ::Array{String}, ::Array{Float64}) = 0.05
getHessianGuessSchlegel(::RotationC, ::Array{String}, ::Array{Float64}) = 0.05

getHessianGuessLindh(::RotationA, ::Array{String}, ::Array{Float64}) = 0.05
getHessianGuessLindh(::RotationB, ::Array{String}, ::Array{Float64}) = 0.05
getHessianGuessLindh(::RotationC, ::Array{String}, ::Array{Float64}) = 0.05

function toString(r::Rotation)
    join(string.(r.indices), ", ")
end

toString(r::RotationA) = "RotationA($(toString(r.rotation)))"
toString(r::RotationB) = "RotationB($(toString(r.rotation)))"
toString(r::RotationC) = "RotationC($(toString(r.rotation)))"