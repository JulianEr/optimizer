struct HydrogenBond <: AbstractInternalCoordinate
    bond::Bond
end

HydrogenBond(a::Int64, b::Int64) = HydrogenBond(Bond(a, b))

function ==(a::HydrogenBond, b::HydrogenBond)
    a.bond == b.bond
end

function ==(a::HydrogenBond, b::Bond)
    a.bond == b
end

function ==(a::Bond, b::HydrogenBond)
    a == b.bond
end

getA(self::HydrogenBond) = self.bond.a
getB(self::HydrogenBond) = self.bond.b

function value(self::HydrogenBond, xs::Array{Float64})
    value(self.bond, xs)
end

function derivativeVector(self::HydrogenBond, xs::Array{Float64})
    derivativeVector(self.bond, xs)
end

function secondDerivativeMatrix(self::HydrogenBond, xs::Array{Float64})
    secondDerivativeMatrix(self.bond, xs)
end

getHessianGuessSchlegel(self::HydrogenBond, symbols::Array{String}, cartesians::Array{Float64}) = getHessianGuessSchlegel(self.bond, symbols, cartesians)

getHessianGuessLindh(self::HydrogenBond, symbols::Array{String}, cartesians::Array{Float64}) = getHessianGuessLindh(self.bond, symbols, cartesians)

toString(b::HydrogenBond) = "HydrogenBond($(b.bond.a), $(b.bond.b))"