import Base: ==

struct Translation 
    indices::Array{Int}
end

struct TranslationX <: AbstractInternalCoordinate
    translation::Translation
end

struct TranslationY <: AbstractInternalCoordinate
    translation::Translation
end

struct TranslationZ <: AbstractInternalCoordinate
    translation::Translation
end

TranslationX(indices::Array{Int}) = TranslationX(Translation(indices))
TranslationY(indices::Array{Int}) = TranslationY(Translation(indices))
TranslationZ(indices::Array{Int}) = TranslationZ(Translation(indices))

function ==(a::TranslationX, b::TranslationX)
    a.translation.indices == b.translation.indices
end

function ==(a::TranslationY, b::TranslationY)
    a.translation.indices == b.translation.indices
end


function ==(a::TranslationZ, b::TranslationZ)
    a.translation.indices == b.translation.indices
end

value(self::TranslationX, xs::Array{Float64}) = value(self.translation, 1, xs)

value(self::TranslationY, xs::Array{Float64}) = value(self.translation, 2, xs)

value(self::TranslationZ, xs::Array{Float64}) = value(self.translation, 3, xs)

function value(self::Translation, direction::Int64, xs::Array{Float64})
    N = length(self.indices)
    sum([xs[(i-1)*3+direction] for i ∈ self.indices])/N
end

function derivative(self::Translation, direction::Int64)
    vec = fill(0.0, 3)
    vec[direction] = 1/length(self.indices)
    vec
end

derivativeVector(self::TranslationX, xs::Array{Float64}) = derivativeVector(self.translation, 1, xs)

derivativeVector(self::TranslationY, xs::Array{Float64}) = derivativeVector(self.translation, 2, xs)

derivativeVector(self::TranslationZ, xs::Array{Float64}) = derivativeVector(self.translation, 3, xs)

function derivativeVector(self::Translation, direction::Int64, xs::Array{Float64})
    vec = fill(0.0, length(xs))
    der = derivative(self, direction)

    for i ∈ self.indices
        vec[(i-1)*3+1:i*3] = der
    end

    vec
end

secondDerivativeMatrix(self::TranslationX, xs::Array{Float64}) = secondDerivativeMatrix(self.translation, xs)

secondDerivativeMatrix(self::TranslationY, xs::Array{Float64}) = secondDerivativeMatrix(self.translation, xs)

secondDerivativeMatrix(self::TranslationZ, xs::Array{Float64}) = secondDerivativeMatrix(self.translation, xs)

function secondDerivativeMatrix(::Translation, xs::Array{Float64})
    fill(0.0, (length(xs), length(xs)))
end

getHessianGuessSchlegel(::TranslationX, ::Array{String}, ::Array{Float64}) = 0.05
getHessianGuessSchlegel(::TranslationY, ::Array{String}, ::Array{Float64}) = 0.05
getHessianGuessSchlegel(::TranslationZ, ::Array{String}, ::Array{Float64}) = 0.05

getHessianGuessLindh(::TranslationX, ::Array{String}, ::Array{Float64}) = 0.05
getHessianGuessLindh(::TranslationY, ::Array{String}, ::Array{Float64}) = 0.05
getHessianGuessLindh(::TranslationZ, ::Array{String}, ::Array{Float64}) = 0.05

function toString(t::Translation)
    join(string.(t.indices), ", ")
end

toString(t::TranslationX) = "TranslationX($(toString(t.translation)))"
toString(t::TranslationY) = "TranslationY($(toString(t.translation)))"
toString(t::TranslationZ) = "TranslationZ($(toString(t.translation)))"