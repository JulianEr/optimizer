struct DefaultLine <: ZmatrixLine
    lineNumber::Int
    bond::Bond
    angle::Angle
    dihedral::Dihedral
end

function getInternalValues(line::DefaultLine, cartesians::Array{Float64})
    [value(line.bond, cartesians); value(line.angle, cartesians); value(line.dihedral, cartesians)]
end

function getBmatrixLines(line::DefaultLine, cartesians::Array{Float64})
    B = Array{Float64, 2}(undef, 3, length(cartesians))
    B[1, :] = derivativeVector(line.bond, cartesians)
    B[2, :] = derivativeVector(line.angle, cartesians)
    B[3, :] = derivativeVector(line.dihedral, cartesians)
    B
end

function getDerivativeOfBmatrixMatrices(line::DefaultLine, cartesians::Array{Float64})
    BB = Array{Float64, 3}(undef, 3, length(cartesians), length(cartesians))
    BB[1, :, :] = secondDerivativeMatrix(line.bond, cartesians)
    BB[2, :, :] = secondDerivativeMatrix(line.angle, cartesians)
    BB[3, :, :] = secondDerivativeMatrix(line.dihedral, cartesians)
    BB
end

function getHessianGuessValues(line::DefaultLine, symbols::Array{String}, cartesians::Array{Float64})
    [getHessianGuessLindh(line.bond, symbols, cartesians); getHessianGuessLindh(line.angle, symbols, cartesians); getHessianGuessLindh(line.dihedral, symbols, cartesians)]
end

function getHessianGuessSchlegelValues(line::DefaultLine, symbols::Array{String}, cartesians::Array{Float64})
    [getHessianGuessSchlegel(line.bond, symbols, cartesians); getHessianGuessSchlegel(line.angle, symbols, cartesians); getHessianGuessSchlegel(line.dihedral, symbols, cartesians)]
end

function printLine(line::DefaultLine, symbols::Array{String}, values::Array{Float64})
    b = line.bond.a
    a = line.angle.a
    d = line.dihedral.a
    @sprintf("%3s %2d %12.7f %2d %12.7f %2d %12.7f", symbols[line.lineNumber], 
                                          b, values[(line.lineNumber-3)*3 + 1]*bohr2Ang,
                                          a, values[(line.lineNumber-3)*3 + 2]*180.0/π,
                                          d, values[(line.lineNumber-3)*3 + 3]*180.0/π)
end

function addToCartesian!(line::DefaultLine, values::Array{Float64}, coordinates::Array{Float64}, startAt::Int=0)
    bond = values[(line.lineNumber-4)*3 + 4]
    angle = values[(line.lineNumber-4)*3 + 5]
    dihedral = values[(line.lineNumber-4)*3 + 6]

    dihedral = dihedral < 0.0 ? dihedral + 2.0*π : dihedral

    reference = getCartesian(line.dihedral.c-startAt, coordinates) - getCartesian(line.dihedral.b-startAt, coordinates)
    reference /= norm(reference)

    referenceNormal = getCartesian(line.dihedral.b-startAt, coordinates) - getCartesian(line.dihedral.a-startAt, coordinates)
    referenceNormal = cross(referenceNormal, reference)
    referenceNormal /= norm(referenceNormal)

    M = [reference cross(referenceNormal, reference) referenceNormal]
    c = M*[-bond*cos(angle);
            bond*sin(angle)*cos(dihedral);
            bond*sin(angle)*sin(dihedral)]

    c += getCartesian(line.dihedral.c-startAt, coordinates)
    coordinates[(line.dihedral.d-1-startAt)*3+1:(line.dihedral.d-1-startAt)*3+3] = c
end