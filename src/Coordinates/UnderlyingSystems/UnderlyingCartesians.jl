mutable struct UnderlyingCartesians <: UnderlyingSystem
    cartesians::Array{Float64}
end