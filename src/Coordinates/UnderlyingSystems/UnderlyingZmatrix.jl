mutable struct UnderlyingZmatrix <: UnderlyingSystem
    zMatrix::AbstractZmatrix
    redundantIndices::Array{Int64}
    cartesians::Array{Float64}
end