#findAllHydrogenBonds(b)

findAllBonds(bondGraph::SimpleGraph{Int}) = [Bond(b...) for b ∈ getBonds(bondGraph)]

findAllAngles(bondGraph::SimpleGraph{Int}) = [Angle(a...) for a ∈ getAngles(bondGraph)]

findAllDihedrals(bondGraph::SimpleGraph{Int}) = [Dihedral(d...) for d ∈ getDihedrals(bondGraph)]

function findTranslationsAndRotations(bondGraph::SimpleGraph{Int}, cartesians::Array{Float64}) 
    molecules = getMolecules(bondGraph)
    x, y, z = findTranslations(molecules)
    a, b, c = findRotations(molecules, cartesians)
    x, y, z, a, b, c
end

function findTranslations(molecules::Array{Array{Int, 1}, 1})
    N = length(molecules)
    x = Array{TranslationX}(undef, N)
    y = Array{TranslationY}(undef, N)
    z = Array{TranslationZ}(undef, N)
    for m ∈ 1:N
        x[m] = TranslationX(molecules[m])
        y[m] = TranslationY(molecules[m])
        z[m] = TranslationZ(molecules[m])
    end
    x, y, z
end

function findRotations(molecules::Array{Array{Int, 1}, 1}, cartesians::Array{Float64})
    N = length(molecules)
    a = Array{RotationA}(undef, N)
    b = Array{RotationB}(undef, N)
    c = Array{RotationC}(undef, N)
    for m ∈ 1:N
        a[m] = RotationA(molecules[m], cartesians)
        b[m] = RotationB(molecules[m], cartesians)
        c[m] = RotationC(molecules[m], cartesians)
    end
    a, b, c
end

function findAllInternalCoordinates(bondGraph::SimpleGraph{Int}, symbols::Array{String}, cartesians::Array{Float64}; withHydrogens::Bool=true, forceTranslationRotation::Bool=false)
    molecules = getMolecules(bondGraph)
    internalCoordinates = begin 
        if forceTranslationRotation || length(molecules) > 1
            findAllTranslationRotationAndPrimitives(bondGraph, cartesians, molecules)
        else
            findAllPrimitives(bondGraph)
        end
    end
    if withHydrogens
        addHydrogenBondsToInternalCoordinates(internalCoordinates, symbols, cartesians)
    else
        internalCoordinates
    end
end

function findAllTranslationRotationAndPrimitives(bondGraph::SimpleGraph{Int}, cartesians::Array{Float64}, molecules::Array{Array{Int, 1}, 1})
    x, y, z = findTranslations(molecules)
    a, b, c = findRotations(molecules, cartesians)
    TranslationRotationAndPrimitives(molecules, x, y, z, a, b, c, findAllPrimitives(bondGraph))
end

findAllPrimitives(bondGraph::SimpleGraph{Int}) = Primitives(findAllBonds(bondGraph), findAllAngles(bondGraph), findAllDihedrals(bondGraph))

function findHydrogensBoundToElectronegativeElements(bonds::Array{Bond}, symbols::Array{String}) 
    foundBonds::Vector{Tuple{Int64, Int64}} = []
    for b ∈ bonds
        symbolA = symbols[getA(b)]
        symbolB = symbols[getB(b)]

        if symbolA == "H" && symbolB in ELECTRO_NEGATIVE_ELEMENTS
            push!(foundBonds, (getB(b), getA(b)))
        elseif symbolB == "H" && symbolA in ELECTRO_NEGATIVE_ELEMENTS
            push!(foundBonds, (getA(b), getB(b)))
        end
    end
    foundBonds
end

distanceQualifiesAsHydrogenBond(hx::String, distance::Float64) = COVALENT_THRESHOLD * covalentDistancesHydrogenToElectronegativeElements[hx] < distance < VDW_THRESHOLD*vdwDistancesHydrogenToElectronegativeElements[hx]

angleQualifiesAsHydrogenBond(angle::Angle, cartesians::Array{Float64}) = value(angle, cartesians) > π/2

function handlePossibleHydrogenBond(possibleHydrogenBond::Tuple{Int64, Int64}, symbols::Array{String}, cartesians::Array{Float64})

    hydrogenBonds::Array{HydrogenBond} = []

    noneHydrogenAtom, hydrogenAtom = possibleHydrogenBond

    for i ∈ 1:length(symbols)
        if i == noneHydrogenAtom || i == hydrogenAtom
            continue
        end

        if symbols[i] in ELECTRO_NEGATIVE_ELEMENTS
            possibleHydrogenBond = HydrogenBond(i, hydrogenAtom)
            dist = value(possibleHydrogenBond, cartesians)
            hx = "H$(symbols[i])"

            if distanceQualifiesAsHydrogenBond(hx, dist) && angleQualifiesAsHydrogenBond(Angle(noneHydrogenAtom, hydrogenAtom, i), cartesians)
                push!(hydrogenBonds, possibleHydrogenBond)
            end
        end
    end

    hydrogenBonds
end

function addHydrogenBondsToInternalCoordinates(internalCoordinates::AbstractSetOfInternalCoordinates, symbols::Array{String}, cartesians::Array{Float64})

    bonds = getBonds(internalCoordinates)
    possibleHydrogenBonds = findHydrogensBoundToElectronegativeElements(bonds, symbols)
    hydrogenBonds::Array{HydrogenBond} = []

    for possibleHydrogenBond ∈ possibleHydrogenBonds 
        append!(hydrogenBonds, handlePossibleHydrogenBond(possibleHydrogenBond, symbols, cartesians))
    end

    InternalCoordinatesWithHydrogenBonds(hydrogenBonds, internalCoordinates)
end
