## TODO enable constraints again
mutable struct ZmatrixInternalCoordinates <: AbstractCoordinates
    internalCoordinates::ZmatrixSetOfInternals
    values::Array{Float64}
    cartesians::Array{Float64}
    B::Array{Float64, 2}
    G⁻¹::Array{Float64}
    P::AbstractArray{Float64, 2}
    function ZmatrixInternalCoordinates(internalCoordinates::AbstractSetOfInternalCoordinates, cartesians::Array{Float64}, ::Primitives=Primitives([], [], []))
        zMatrix, _ = zMatrixForRedundantInternalCoordinates(internalCoordinates)
        internalCoordinates = ZmatrixSetOfInternals(zMatrix)
        B = getBmatrix(internalCoordinates, cartesians)
        G⁻¹ = pinv(B*B', 1e-7)
        #if isempty(constraints)
        new(internalCoordinates, getValuesFromCartesians(internalCoordinates, cartesians), cartesians, B, G⁻¹, Array{Float64}(undef, 0, 0))
        # else
        #     p = fill(1.0, getInternalsSize(zMatrix))
        #     for c ∈ constraints
        #         p[c] = 0
        #     end
        #     new(zMatrix, cartesians, B, G⁻¹, Diagonal(p))
        # end
    end
end

function setCoordinates!(self::ZmatrixInternalCoordinates, newValues::Array{Float64})
    self.values = newValues
    self.cartesians = toCartesians(self.internalCoordinates.zMatrix, newValues)

    self.B = getBmatrix(self.internalCoordinates, self.cartesians)
    self.G⁻¹ = pinv(self.B*self.B', 1e-7)
end

function applyChange!(self::ZmatrixInternalCoordinates, Δxs::Array{Float64})
    @debug "Applying Change:" Δxs=Δxs
    newValues = self.values + Δxs
    newValues = removeOverwind(self.internalCoordinates, newValues)
    setCoordinates!(self, newValues)
end

function getChangeFromPrimitives(self::ZmatrixInternalCoordinates, primitiveValues::Array{Float64})
    getDifferences(self.internalCoordinates, self.values, primitiveValues)
end

getProjectedValues(self::ZmatrixInternalCoordinates) = self.values

getPrimitiveValues(self::ZmatrixInternalCoordinates) = self.values

getCartesians(self::ZmatrixInternalCoordinates) = self.cartesians

function transformCartesianGradients(self::ZmatrixInternalCoordinates, g::Array{Float64}) 
    f = self.G⁻¹*self.B*g
    if isempty(self.P)
        f
    else
        self.P*f
    end
end

function transformCartesianHessian(self::ZmatrixInternalCoordinates, H::AbstractArray{Float64, 2}, g::Array{Float64}, cartesians::Array{Float64})
    transformCartesianHessian(self.internalCoordinates, H, g, self.B, self.G⁻¹, cartesians)
end

function gradientsToActiveSpace(self::ZmatrixInternalCoordinates, g::Array{Float64}) 
    @debug "Gradients:" g=g
    if isempty(self.P)    
        g
    else
        self.P*g
    end
end

function hessianToActiveSpace(self::ZmatrixInternalCoordinates, H::AbstractArray{Float64, 2}) 
    @debug "Hessian:" H=H
    if isempty(self.P)
        H
    else
        Hermitian(self.P*H*self.P)
    end
end

function weightsToHessian(self::ZmatrixInternalCoordinates, H::AbstractArray{Float64, 2})
    if isempty(self.P)
        H
    else
        Hermitian(H + 1000*(I - self.P))
    end
end

function guessHessianLindh(self::ZmatrixInternalCoordinates, symbols::Array{String}, ::AbstractOptimizer) 
    Hermitian(guessHessianLindh(self.internalCoordinates, symbols, self.cartesians))
end

function guessHessianSchlegel(self::ZmatrixInternalCoordinates, symbols::Array{String}, ::AbstractOptimizer)
    Hermitian(guessHessianSchlegel(self.internalCoordinates, symbols, self.cartesians))
end

function identityHessian(self::ZmatrixInternalCoordinates)
    Hermitian(1.0I(getFullInternalsSize(self.internalCoordinates)))
end

resetCoordinateSystem!(::ZmatrixInternalCoordinates) = error("Z-Matrix Coordinates were tried to reset. I cannot reset Z-Matrix Coordinates. Aborting.")