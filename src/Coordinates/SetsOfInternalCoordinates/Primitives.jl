mutable struct Primitives <: AbstractSetOfInternalCoordinates
    bonds::Array{Bond}
    angles::Array{Angle}
    dihedrals::Array{Dihedral}
end

function coordsForRedundantInternalCoordinates(self::Primitives, cartesians::Array{Float64}, internalsOffset::Int64)
    @info "Using an internal coordinates set without translation and rotation."
    try
        lines, redundantIndicesTuples = zMatrixForRedundantInternalCoordinates(self)
        UnderlyingZmatrix(lines, [i+internalsOffset for indices ∈ redundantIndicesTuples for i ∈ indices if i != 0], cartesians)
    catch execption
        @info "$(execption.msg).\nThat did not work out, using cartesian coordinates for change transformation."
        UnderlyingCartesians(cartesians)
    end
end

getThisInternalsSize(self::Primitives) = length(self.bonds) + length(self.angles) + length(self.dihedrals)
getFullInternalsSize(self::Primitives) = getThisInternalsSize(self)

getBonds(internalCoordinates::Primitives) = internalCoordinates.bonds

function removeUndefinedDihedrals(self::Primitives, cartesians::Array{Float64})
    self.dihedrals = [d for d in self.dihedrals if isDefined(d, cartesians)]
end

function addToBmatrix!(self::Primitives, cartesians::Array{Float64}, B::Array{Float64, 2}, internalsOffset::Int64)
    nb, na, nd = length(self.bonds), length(self.angles), length(self.dihedrals)
    fromBonds, toBonds = internalsOffset+1, internalsOffset+nb
    fromAngles, toAngles = toBonds+1, toBonds+na
    fromDihedrals, toDihedrals = toAngles+1, toAngles+nd
    for (index, i) ∈ enumerate(fromBonds:toBonds)
        B[i,:] = derivativeVector(self.bonds[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromAngles:toAngles)
        B[i, :] = derivativeVector(self.angles[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromDihedrals:toDihedrals)
        B[i, :] = derivativeVector(self.dihedrals[index], cartesians)
    end
end

function addToBmatrixDerivative!(self::Primitives, cartesians::Array{Float64}, BB::Array{Float64, 3}, internalsOffset::Int64)
    nb, na, nd = length(self.bonds), length(self.angles), length(self.dihedrals)
    fromBonds, toBonds = internalsOffset+1, internalsOffset+nb
    fromAngles, toAngles = toBonds+1, toBonds+na
    fromDihedrals, toDihedrals = toAngles+1, toAngles+nd
    for (index, i) ∈ enumerate(fromBonds:toBonds)
        BB[i,:,:] = secondDerivativeMatrix(self.bonds[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromAngles:toAngles)
        BB[i,:,:] = secondDerivativeMatrix(self.angles[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromDihedrals:toDihedrals)
        BB[i,:,:] = secondDerivativeMatrix(self.dihedrals[index], cartesians)
    end
end

function addToHessianGuessLindh!(self::Primitives, symbols::Array{String}, cartesians::Array{Float64}, h::Array{Float64}, internalsOffset::Int64)
    nb, na, nd = length(self.bonds), length(self.angles), length(self.dihedrals)
    fromBonds, toBonds = internalsOffset+1, internalsOffset+nb
    fromAngles, toAngles = toBonds+1, toBonds+na
    fromDihedrals, toDihedrals = toAngles+1, toAngles+nd
    for (index, i) ∈ enumerate(fromBonds:toBonds)
        h[i] = getHessianGuessLindh(self.bonds[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromAngles:toAngles)
        h[i] = getHessianGuessLindh(self.angles[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromDihedrals:toDihedrals)
        h[i] = getHessianGuessLindh(self.dihedrals[index], symbols, cartesians)
    end
end

function addToHessianGuessSchlegel!(self::Primitives, symbols::Array{String}, cartesians::Array{Float64}, h::Array{Float64}, internalsOffset::Int64)
    nb, na, nd = length(self.bonds), length(self.angles), length(self.dihedrals)
    fromBonds, toBonds = internalsOffset+1, internalsOffset+nb
    fromAngles, toAngles = toBonds+1, toBonds+na
    fromDihedrals, toDihedrals = toAngles+1, toAngles+nd
    for (index, i) ∈ enumerate(fromBonds:toBonds)
        h[i] = getHessianGuessSchlegel(self.bonds[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromAngles:toAngles)
        h[i] = getHessianGuessSchlegel(self.angles[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromDihedrals:toDihedrals)
        h[i] = getHessianGuessSchlegel(self.dihedrals[index], symbols, cartesians)
    end
end

function removeOverwindStartingAtOffset!(self::Primitives, values::Array{Float64}, internalsOffset::Int64)
    nb, na, nd = length(self.bonds), length(self.angles), length(self.dihedrals)
    startDihedrals = internalsOffset + nb + na + 1
    endDihedrals = startDihedrals + nd - 1
    for i ∈ startDihedrals:endDihedrals
        values[i] = keepPiInRange(values[i])
    end
end

function addValuesToCoordinates!(self::Primitives, cartesians::Array{Float64}, values::Array{Float64}, internalsOffset::Int64)
    nb, na, nd = length(self.bonds), length(self.angles), length(self.dihedrals)
    fromBonds, toBonds = internalsOffset+1, internalsOffset+nb
    fromAngles, toAngles = toBonds+1, toBonds+na
    fromDihedrals, toDihedrals = toAngles+1, toAngles+nd
    for (index, i) ∈ enumerate(fromBonds:toBonds)
        values[i] = value(self.bonds[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromAngles:toAngles)
        values[i] = value(self.angles[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromDihedrals:toDihedrals)
        values[i] = value(self.dihedrals[index], cartesians)
    end
end

function addAndFindConstraints!(self::Primitives, constraints::TrAndPrimitiveConstraints, cartesians::Array{Float64})
    newCoordinateSystem = TranslationRotationAndPrimitives([], [], [], [], [], [], [], self)
    addAndFindConstraints!(newCoordinateSystem, constraints, cartesians)
end

function addAndFindConstraints!(self::Primitives, constraints::TrAndPrimitiveConstraints, cartesians::Array{Float64}, listOfConstraints::Array{Int64}, internalsOffset::Int64)
    addAndFindConstraints!(self, constraints.primitives, cartesians, listOfConstraints, internalsOffset)
end

function addAndFindConstraints!(self::Primitives, constraints::PrimitiveConstraints, ::Array{Float64}, listOfConstraints::Array{Int64}, internalsOffset::Int64)
    for b ∈ constraints.bonds
        index = findfirst(x -> x == b, self.bonds)
        if index !== nothing
            push!(listOfConstraints, internalsOffset + index)
        else
            push!(self.bonds, Bond(b.a, b.b))
            push!(listOfConstraints, internalsOffset + length(self.bonds))
        end
    end
    bondsEnd = internalsOffset + length(self.bonds)
    for a ∈ constraints.angles
        index = findfirst(x -> x == a, self.angles)
        if index !== nothing
            push!(listOfConstraints, bondsEnd + index)
        else
            push!(self.angles, Angle(a.a, a.b, a.c))
            push!(listOfConstraints, bondsEnd + length(self.angles))
        end
    end
    anglesEnd = bondsEnd + length(self.angles)
    for d ∈ constraints.dihedrals
        index = findfirst(x -> x == d, self.dihedrals)
        if index !== nothing
            push!(listOfConstraints, anglesEnd + index)
        else
            push!(self.dihedrals, Dihedral(d.a, d.b, d.c, d.d))
            push!(listOfConstraints, anglesEnd + length(self.dihedrals))
        end
    end
end

hasTranslationAndRotation(::Primitives, tr::Bool) = tr

function toStringWithOffset(self::Primitives, values::Array{Float64}, internalsOffset::Int64)
    result = ""
    
    nb, na, nd = length(self.bonds), length(self.angles), length(self.dihedrals)
    fromBonds, toBonds = internalsOffset+1, internalsOffset+nb
    fromAngles, toAngles = toBonds+1, toBonds+na
    fromDihedrals, toDihedrals = toAngles+1, toAngles+nd

    for (index, i) ∈ enumerate(fromBonds:toBonds)
        result *= toString(self.bonds[index])*@sprintf("%15.10f\n", values[i])
    end
    for (index, i) ∈ enumerate(fromAngles:toAngles)
        result *= toString(self.angles[index])*@sprintf("%15.10f\n", values[i])
    end
    for (index, i) ∈ enumerate(fromDihedrals:toDihedrals)
        result *= toString(self.dihedrals[index])*@sprintf("%15.10f\n", values[i])
    end

    result
end