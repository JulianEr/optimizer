const readCartesianXyzFilePattern = r"([A-Z][a-u]{0,2}\b)\s+([+-]?\d+\.\d+[eE]?[+-]?\d*\b)\s+([+-]?\d+\.\d+[eE]?[+-]?\d*\b)\s*([+-]?\d+\.\d+[eE]?[+-]?\d*\b)"

function readFromAngCartesiansToBohr(filename::String)
    fileContent = open(filename) do iFile
        read(iFile, String)
    end
    readFromAngCartesiansContentToBohr(fileContent)
end

function readFromAngCartesiansContentToBohr(content::String)
    symbols = Array{String}(undef,0)
    coordinates = Array{Float64}(undef,0)
    for match ∈ eachmatch(readCartesianXyzFilePattern, content)
        push!(symbols, match[1])
        for i ∈ 2:4
            push!(coordinates, parse(Float64, match[i]))
        end
    end

    symbols, ang2Bohr(coordinates)
end

function writeAngCartesiansFromBohr(symbols::Array{String}, coordinates::Array{Float64})
    angCoordinates = borh2Ang(coordinates)
    xyz = @sprintf("%d\n\n", length(angCoordinates)÷3)
    for (i, s) ∈ enumerate(symbols)
        xyz *= @sprintf("%s %25.15f %25.15f %25.15f\n", s, angCoordinates[(i-1)*3+1], angCoordinates[(i-1)*3+2], angCoordinates[(i-1)*3+3])
    end
    xyz
end