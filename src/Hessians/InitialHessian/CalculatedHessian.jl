struct CalculatedHessian <: AbstractInitialHessian end

function getInitialHessian(::CalculatedHessian, optimizer::AbstractOptimizer)
    @info "Calculate Hessian"
    H = getAnalyticHessian(optimizer)
    @info "Hessian:" H=H
    H
end