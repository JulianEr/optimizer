struct GuessHessian <: AbstractInitialHessian 
    symbols::Array{String}
    guessType::String
end

GuessHessian(symbols::Array{String}) = GuessHessian(symbols, "Schlegel")

function getInitialHessian(guess::GuessHessian, optimizer::AbstractOptimizer)
    if guess.guessType == "Schlegel"
        guessHessianSchlegel(getCoordinateSystem(optimizer), guess.symbols, optimizer)
    elseif guess.guessType == "Lindh"
        guessHessianLindh(getCoordinateSystem(optimizer), guess.symbols, optimizer)
    end
end