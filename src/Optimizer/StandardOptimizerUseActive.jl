mutable struct StandardOptimizerUseActive <: AbstractOptimizer
    optimizer::StandardOptimizer
    function StandardOptimizerUseActive(optimizer::StandardOptimizer)
        new(optimizer) 
    end
end

getEnergy(self::StandardOptimizerUseActive) = getEnergy(self.optimizer)
getFormerEnergy(self::StandardOptimizerUseActive) = getFormerEnergy(self.optimizer)
getCartesians(self::StandardOptimizerUseActive) = getCartesians(self.optimizer)
getFormerCartesians(self::StandardOptimizerUseActive) = getFormerCartesians(self.optimizer)
getChange(self::StandardOptimizerUseActive) = getChange(self.optimizer)
getPrimitiveValues(self::StandardOptimizerUseActive) = getPrimitiveValues(self.optimizer)
transformCartesianGradients(self::StandardOptimizerUseActive, cartesianG::Array{Float64}) = transformCartesianGradients(self.optimizer, cartesianG)
transformCartesianHessian(self::StandardOptimizerUseActive, cartesianH::AbstractArray{Float64, 2}, cartesianG::Array{Float64}) = transformCartesianHessian(self.optimizer, cartesianH, cartesianG)
getCoordinateSystem(self::StandardOptimizerUseActive) = getCoordinateSystem(self.optimizer)
getGradients(self::StandardOptimizerUseActive) = getGradients(self.optimizer)
getActiveGradients(self::StandardOptimizerUseActive) = getGradients(self.optimizer)
getHessian(self::StandardOptimizerUseActive) = getHessian(self.optimizer)
getActiveHessian(self::StandardOptimizerUseActive) = weightsToHessian(self.optimizer.xs, getHessian(self.optimizer))

takeSnapShot(self::StandardOptimizerUseActive) = takeSnapShot(self.optimizer)
resetToSnapShot!(self::StandardOptimizerUseActive, data::DataOfFormersStandardOptimizer) = resetToSnapShot!(self.optimizer, data)
resetCoordinateSystem!(self::StandardOptimizerUseActive) = resetCoordinateSystem!(self.optimizer)

function isConvergenceReached(self::StandardOptimizerUseActive) 
    isConvergenceReached(self.optimizer.convergenceCheck, self)
end

function getNewEnergyAndGradients!(self::StandardOptimizerUseActive) 
    getNewEnergyAndGradients!(self.optimizer)
    self.optimizer.gradients = gradientsToActiveSpace(self.optimizer.xs, self.optimizer.gradients)
end

savePoint(self::StandardOptimizerUseActive) = savePoint(self.optimizer)

function initialize!(self::StandardOptimizerUseActive) 
    getNewEnergyAndGradients!(self)
    self.optimizer.hessian = hessianToActiveSpace(self.optimizer.xs, getInitialHessian(self.optimizer.initialHessian, self))
    @info "The initial Hessian:" H=self.optimizer.hessian
    notify!(self.optimizer.gradientsAndHessian, self)
end

function takeStep(self::StandardOptimizerUseActive)
    takeStep!(self.optimizer.stepControl, self)
end

applyChange!(self::StandardOptimizerUseActive, Δxs::Array{Float64}) = applyChange!(self.optimizer, Δxs)

function updateTrustRadius!(self::StandardOptimizerUseActive)
    updateTrustRadius(self.optimizer.trustRadiusUpdate, self.optimizer.stepControl, getEnergy(self.optimizer) - getFormerEnergy(self.optimizer))
end

function getNewHessian!(self::StandardOptimizerUseActive)
    getNewHessian!(self.optimizer)
    self.optimizer.hessian = hessianToActiveSpace(self.optimizer.xs, self.optimizer.hessian)
end

function getAnalyticHessian(self::StandardOptimizerUseActive) 
    H = getAnalyticHessian(self.optimizer)
    hessianToActiveSpace(self.optimizer.xs, H)
end

executeAfterStep!(self::StandardOptimizerUseActive) = executeAfterStep!(self.optimizer)