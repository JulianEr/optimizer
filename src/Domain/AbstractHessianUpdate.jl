# COV_EXCL_START
notify!(self::AbstractHessianUpdate, ::AbstractOptimizer) = error("\"notify!\" not Implemented for type $(typeof(self))")
updateHessian(self::AbstractHessianUpdate, ::AbstractOptimizer) = error("\'updateHessian\' not Implemented for type $(typeof(self))")
# COV_EXCL_STOP