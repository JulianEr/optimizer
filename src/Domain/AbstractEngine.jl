# COV_EXCL_START
getEnergyAndGradients(self::AbstractEngine, ::Array{String}, ::Array{Float64}) = error("\'getEnergyAndGradients\' not Implemented for type $(typeof(self))")
getGradientsAndHessian(self::AbstractEngine, ::Array{String}, ::Array{Float64}) = error("\'getGradientsAndHessian\' not Implemented for type $(typeof(self))") 
# COV_EXCL_STOP