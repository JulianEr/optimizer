abstract type AbstractOptimizer end

abstract type AbstractInitialHessian end

abstract type AbstractConvergenceCheck end

abstract type AbstractEngine end

abstract type AbstractZmatrix end

abstract type ZmatrixLine end

abstract type AbstractGradientsAndHessian end

abstract type AbstractHessianUpdate end

abstract type AbstractInternalCoordinate end

abstract type Constraint end

abstract type SetOfConstraints end

abstract type AbstractSetOfInternalCoordinates end

abstract type AbstractCoordinates end

abstract type UnderlyingSystem end

abstract type AbstractStepControl end

abstract type AbstractTrustRadiusUpdate end

abstract type AbstractOptimizationAlgorithm end

abstract type ResetData end

abstract type Period end