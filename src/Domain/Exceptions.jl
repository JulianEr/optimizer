struct CoordinateTransformationException <: Exception end

showerror(io::IO, ::CoordinateTransformationException) = print(io, "Failed on transformation setp.")