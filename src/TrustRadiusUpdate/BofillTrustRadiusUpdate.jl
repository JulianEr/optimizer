using Printf

struct BofillTrustRadiusUpdate <: AbstractTrustRadiusUpdate 
    lowerBound_ρ::Float64
    upperBound_ρ::Float64
    lowerOuterBound::Float64
    lowerInnerBound::Float64
    upperInnerBound::Float64
    upperOuterBound::Float64
    scalingFactor::Float64
    thresholdForTrustRadiusFloat::Float64
end

BofillTrustRadiusUpdate(
    lowerBound_ρ::Float64 = 0.0, 
    upperBound_ρ::Float64 = 2.0,
    scalingFactor::Float64 = 2.0,
    smallNumberBound::Float64 = 0.75, 
    highNumberBound::Float64 = 0.8,
    thresholdForTrustRadiusFloat::Float64 = 1.0e-4) = BofillTrustRadiusUpdate(
        lowerBound_ρ, upperBound_ρ,
        lowerBound_ρ+smallNumberBound,
        lowerBound_ρ+highNumberBound,
        upperBound_ρ-highNumberBound,
        upperBound_ρ-smallNumberBound,
        scalingFactor,
        thresholdForTrustRadiusFloat)

function updateTrustRadius(self::BofillTrustRadiusUpdate, stepControl::AbstractStepControl, energyChange::Float64)
    ρ = energyChange/getPredictedEnergyChnage(stepControl)
    if ρ > self.upperOuterBound || ρ < self.lowerOuterBound
        stepControl.trustRadius = stepControl.lastNorm / self.scalingFactor
    elseif self.lowerInnerBound <= ρ <= self.upperInnerBound && abs(stepControl.lastNorm - stepControl.trustRadius) < self.thresholdForTrustRadiusFloat
        stepControl.trustRadius *= sqrt(self.scalingFactor)
    end
    @info(@sprintf("Energy: %15.10e, trustRadius: %15.10e, quality: %5.2f\n", energyChange, stepControl.trustRadius, ρ))
    if abs(stepControl.trustRadius-self.thresholdForTrustRadiusFloat) < 1.0e-6
        @warn("Cannot reject step because TR is at its minimum: (TR) $(stepControl.trustRadius) == (Min) $(self.minTrustRadius)")
        return true
    elseif ρ < self.lowerBound_ρ
        @warn("Rejecting step! ρ = $(ρ) which is lower than the lower bound: $(self.lowerBound_ρ).")
        return false
    elseif ρ > self.upperBound_ρ
        @warn("Rejecting step! ρ = $(ρ) which is higher than the upper bound: $(self.upperBound_ρ).")
        return false
    end
    return true
end