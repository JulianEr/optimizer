mutable struct ScalingNewton <: AbstractStepControl 
    trustRadius::Float64
    saddlePointOrder::Int64
    lastNorm::Float64
    modelEnergyChange::Float64
end

ScalingNewton(trustRadius::Float64 = 0.3, saddlePointOrder::Int64 = 0) = ScalingNewton(trustRadius, saddlePointOrder, Inf, Inf)

getPredictedEnergyChnage(self::ScalingNewton) = self.modelEnergyChange

function takeStep!(self::ScalingNewton, optimizer::AbstractOptimizer)
    gradients, hessian = getActiveGradients(optimizer), getActiveHessian(optimizer)
    
    h, U = eigen(hessian)
    f = transpose(U)*gradients

    for i ∈ 1:self.saddlePointOrder
        f[i] = abs(h[i]) > 1.0e-6 ? f[i] : 0
        h[i] = -abs(h[i])
    end
    for i ∈ self.saddlePointOrder+1:length(h)
        f[i] = abs(h[i]) > 1.0e-6 ? f[i] : 0
        h[i] = abs(h[i])
    end

    Δx = -U*(f ./ h)
    self.lastNorm = norm(Δx)

    if self.lastNorm > self.trustRadius
        @info @sprintf "Step with length %9.5f exceeded Trust Radius of %9.5f." self.lastNorm self.trustRadius
        Δx *= (self.trustRadius/self.lastNorm)
        self.lastNorm = self.trustRadius
    end

    self.modelEnergyChange = f'*U'*Δx + 0.5*Δx'*U*Diagonal(h)*U'*Δx

    Δx
end
