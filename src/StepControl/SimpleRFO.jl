mutable struct SimpleRFO <: AbstractStepControl
    saddlePointOrder::Int64
    modelEnergyChange::Float64
    useNewtonModelEnergyChange::Bool
end

SimpleRFO(;saddlePointOrder::Int64 = 0, useNewtonModelEnergyChange::Bool = false) = SimpleRFO(saddlePointOrder, Inf, useNewtonModelEnergyChange)

getPredictedEnergyChnage(self::SimpleRFO) = self.modelEnergyChange

function takeStep!(self::SimpleRFO, optimizer::AbstractOptimizer)
    gradients, hessian = getActiveGradients(optimizer), getActiveHessian(optimizer)
    Δxs, self.modelEnergyChange = begin
        if self.saddlePointOrder == 0 #|| self.saddlePointOrder == length(gradients)
            rfo(gradients, hessian)
        else
            error("Partitioned RFO not implemented yet!")
            #partitionedRfo(self.saddlePointOrder, gradients, hessian)
        end
    end
    Δxs
end