mutable struct RSRFO <: AbstractStepControl
    trustRadius::Float64
    ε₁::Float64
    ε₂::Float64
    saddlePointOrder::Int64
    lastNorm::Float64
    modelEnergyChange::Float64
    useNewtonModelEnergyChange::Bool
end

RSRFO(;trustRadius::Float64 = 0.3, ε₁::Float64 = 0.1, ε₂::Float64 = 1.e-6, saddlePointOrder::Int64=0, useNewtonModelEnergyChange::Bool=false) = RSRFO(trustRadius, ε₁, ε₂, saddlePointOrder, Inf, Inf, useNewtonModelEnergyChange)

getPredictedEnergyChnage(self::RSRFO) = self.modelEnergyChange

function solveRfoEigenValueProblem(rfo::AbstractArray{Float64, 2}, ɑ::Float64)

    S⁻¹² = [1.0; fill(ɑ^-0.5, size(rfo, 1)-1)]
    S⁻¹² = Diagonal(S⁻¹²)

    rfo = S⁻¹² * rfo * S⁻¹²

    λs, zs = eigen(Hermitian(rfo))

    λs, S⁻¹² * zs
end

function rsrfo(self::RSRFO, gradients::Array{Float64}, hessian::AbstractArray{Float64, 2})
    ɑᵢ = 1.0
    ɑᵢ₋₁ = ɑᵢ
    i::Int64 = 0

    h, U = eigen(Hermitian(hessian))
    f = transpose(U)*gradients
    #f[abs.(h).<1.0e-6] .= 0
    Δxs = Vector{Float64}(undef, size(f, 1))
    self.modelEnergyChange = Inf

    while true
        rfo = getRfoMatrix(f, h)

        @debug "RFO-Matrix" rfo=rfo

        λs, νs = solveRfoEigenValueProblem(rfo, ɑᵢ)

        @debug "Eigenvalues and first elements of eigenvectors:" λs=λs νs_firstRow=νs[1,:]

        eigenvec_index = getLowestNonZeroEigenvalue(νs)

        @info "Choosing Eigenvalue and vec(up to 1:n):" i=eigenvec_index λs=λs[1:eigenvec_index]

        if λs[eigenvec_index] >= 0
            @warn "The eigenvalue to the eigenvector starting with a value closest to 1 is not negative. Something went wrong" index = eigenvec_index λ=λs[1:eigenvec_index] ν_starts=νs[1,1:eigenvec_index] ν=νs[:,eigenvec_index]
        end

        Δxs = U * νs[2:end,eigenvec_index] ./ νs[1,eigenvec_index]

        self.lastNorm = norm(Δxs)
        
        if self.useNewtonModelEnergyChange
            self.modelEnergyChange = (transpose(gradients)*Δxs + 0.5*transpose(Δxs)*hessian*Δxs) / (1.0 + transpose(Δxs)*Δxs)
        else
            self.modelEnergyChange = 0.5*(λs[eigenvec_index]/(νs[1,eigenvec_index]^2.0))
        end



        @info "Before Micro steps:" ɑᵢ=ɑᵢ λ=λs[eigenvec_index] ν=λs[eigenvec_index]*ɑᵢ ɑᵢ₋₁=ɑᵢ₋₁ normΔxy=self.lastNorm τ=self.trustRadius absΔxyMinusτ=abs(self.lastNorm-self.trustRadius) ɑᵢ₋₁Minusɑ=ɑᵢ₋₁ - ɑᵢ
        if (i==0 && self.lastNorm > self.trustRadius) || (self.lastNorm > (self.trustRadius+1e-5) && abs(ɑᵢ₋₁ - ɑᵢ) > self.ε₂)
            ∂norm_∂ɑ = λs[eigenvec_index] / (self.lastNorm * (1.0 + self.lastNorm^2.0 * ɑᵢ)) * sum((f.^2.0)./((h .- (λs[eigenvec_index]*ɑᵢ)).^3.0))

            ɑᵢ₋₁ = ɑᵢ
            ɑᵢ -= ((self.lastNorm/self.trustRadius) * ((self.lastNorm - self.trustRadius)/∂norm_∂ɑ))

            @info("Micro step $(i+1):\n\tɑᵢ = $(ɑᵢ); λ = $(λs[eigenvec_index]); ν = $(λs[eigenvec_index]*ɑᵢ);\n\tɑᵢ₋₁=$(ɑᵢ₋₁); |Δxy| = $(self.lastNorm); abs(|Δxy| - τ)=$(abs(self.lastNorm-self.trustRadius)); |ɑᵢ₋₁ - ɑ|=$(ɑᵢ₋₁ - ɑᵢ)")
        else
            break
        end

        i += 1
        if i == 50
            @warn("Optimization step exceeded microiterations.")
            break
        elseif ɑᵢ < 0.0
            @warn("ɑᵢ grew smaller than zero. Terminating microiterations.")
            break
        end
    end
    Δxs
end

# TODO not implemented yet. Rather check for following a eigenvector.
# function prsrfo(self::RSRFO, gradients::Array{Float64}, hessian::AbstractArray{Float64, 2})
#     n = self.saddlePointOrder == 0 ? 1 : self.saddlePointOrder

#     ɑᵢ = 1.0
#     ɑᵢ₋₁ = ɑᵢ
#     i::Int64 = 0

#     h, U = eigen(Hermitian(hessian))
#     f = transpose(U)*gradients
#     Δxs = Vector{Float64}(undef, size(f, 1))
#     self.modelEnergyChange = Inf

#     while true
#         rfoMax, rfoMin = getMaxAndMinRFO(n, f, h)

#         λs_max, νs_max = solveRfoEigenValueProblem(rfoMax, ɑᵢ)
#         λs_min, νs_min = solveRfoEigenValueProblem(rfoMin, ɑᵢ)

#         if abs(νs[1,n]) > 1e-6
#             νs[2:end,n] ./ νs[1,n]
#         else
#             @warn "RFO eigenvector cannot be scaled to its first component νs[1,$(n)] because it is too close to 0" νs1=νs[1,n]
#             νs[2:end,n]
#         end

#         Δxs_max = if abs(νs_max[1,end]) > 1e-6
#                 νs_max[2:end,end] ./ νs_max[1,end]
#             else
#                 νs_max[2:end,end]
#             end
#         Δxs_min = if abs(νs_min[1,1])
#             νs_min[2:end,1]   ./ νs_min[1,1]
#             else
#                 νs_min[2:end,1]
#             end

#         Δxs = U * [Δxs_max;
#                    Δxs_min]

#         self.lastNorm = norm(Δxs)
        
#         self.modelEnergyChange = 0.5*((λs_max[end]/(νs_max[1,end]^2.0)) + (λs_min[1]/(νs_min[1,1])^2.0))
        
#         if (i==0 && self.lastNorm > self.trustRadius) || (abs(self.lastNorm/self.trustRadius - 1.0) > self.ε₁ && abs(ɑᵢ₋₁ - ɑᵢ) > self.ε₂)

#             # short Explanation
#             # ∂norm_∂ɑ = ∂norm_∂Δ² * ∂Δ²_∂ɑ = 1/2 * 1/norm * ∂Δ²_∂ɑ
#             # ∂Δ²_∂ɑ = ∂Δ²_∂ɑ_max + ∂Δ²_∂ɑ_min


#             maxDot = transpose(Δxs_max) * Δxs_max
#             minDot = transpose(Δxs_min) * Δxs_min
#             ∂norm_∂ɑ = let ∂Δ²_∂ɑ_max = 2.0 * (λs_max[end] / (1.0 + maxDot * ɑᵢ)) *
#                                         sum((f[1:n].^2.0)./((h[1:n] .- (λs_max[end]*ɑᵢ)).^3.0)),
#                            ∂Δ²_∂ɑ_min = 2.0 * (λs_min[1] / (1.0 + minDot * ɑᵢ)) *
#                                         sum((f[n+1:end].^2.0)./((h[n+1:end] .- (λs_min[1]*ɑᵢ)).^3.0))

#                 # ∂norm_∂ɑ = ∂norm_∂Δ² * ∂Δ²_∂ɑ = 1/2 * 1/norm * ∂Δ²_∂ɑ
#                 1.0/(2.0 * self.lastNorm) * (∂Δ²_∂ɑ_max + ∂Δ²_∂ɑ_min)
#             end

#             ɑᵢ₋₁ = ɑᵢ
#             ɑᵢ -= ((self.lastNorm/self.trustRadius) * ((self.lastNorm - self.trustRadius)/∂norm_∂ɑ))

#             @info("Micro step $(i+1):\n\tɑᵢ = $(ɑᵢ); λ = $(λs[n]); ν = $(λs[n]*ɑᵢ);\n\tɑᵢ₋₁=$(ɑᵢ₋₁); |Δxy| = $(self.lastNorm); abs(|Δxy| - τ)=$(abs(self.lastNorm-self.trustRadius)); |ɑᵢ₋₁ - ɑ|=$(ɑᵢ₋₁ - ɑᵢ)")
#         else
#             break
#         end

#         i += 1
#         if i == 50
#             @warn("Optimization step exceeded microiterations.")
#             break
#         elseif ɑᵢ < 0.0
#             @warn("ɑᵢ grew smaller than zero. Terminating microiterations.")
#             break
#         end
#     end

#     Δxs
# end

function takeStep!(self::RSRFO, optimizer::AbstractOptimizer)
    gradients, hessian = getActiveGradients(optimizer), getActiveHessian(optimizer)
    if self.saddlePointOrder == 0 || self.saddlePointOrder == length(gradients)
        return rsrfo(self, gradients, hessian)
    else
        return prsrfo(self, gradients, hessian)
    end
end