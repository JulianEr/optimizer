mutable struct ScalingRFO <: AbstractStepControl 
    trustRadius::Float64
    saddlePointOrder::Int64
    lastNorm::Float64
    modelEnergyChange::Float64
    useNewtonModelEnergyChange::Bool
end

ScalingRFO(;trustRadius::Float64 = 0.3, saddlePointOrder::Int64 = 0, useNewtonModelEnergyChange::Bool = false) = ScalingRFO(trustRadius, saddlePointOrder, Inf, Inf, useNewtonModelEnergyChange)

getPredictedEnergyChnage(self::ScalingRFO) = self.modelEnergyChange

function takeStep!(self::ScalingRFO, optimizer::AbstractOptimizer)
    gradients, hessian = getActiveGradients(optimizer), getActiveHessian(optimizer)
    Δxs, self.modelEnergyChange = begin
        if self.saddlePointOrder == 0 # || self.saddlePointOrder == length(gradients)
            rfo(gradients, hessian)
        else
            error("Partitioned RFO not implemented yet!")
            #partitionedRfo(self.saddlePointOrder, gradients, hessian)
        end
    end

    self.lastNorm = norm(Δxs)
    if self.lastNorm > self.trustRadius
        @info @sprintf "Step with length %9.5f exceeded Trust Radius of %9.5f." self.lastNorm self.trustRadius
        Δxs *= self.trustRadius/self.lastNorm
        self.lastNorm = self.trustRadius
        self.modelEnergyChange = (transpose(gradients)*Δxs + 0.5*transpose(Δxs)*hessian*Δxs)
    end

    Δxs
end