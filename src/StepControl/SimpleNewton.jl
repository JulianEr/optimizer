mutable struct SimpleNewton <: AbstractStepControl 
    modelEnergyChange::Float64
end

SimpleNewton() = SimpleNewton(Inf)

getPredictedEnergyChnage(self::SimpleNewton) = self.modelEnergyChange

function takeStep!(self::SimpleNewton, optimizer::AbstractOptimizer)
    gradients, hessian = getActiveGradients(optimizer), getActiveHessian(optimizer)
    h, U = eigen(hessian)
    f = transpose(U)*gradients

    for i ∈ 1:length(h)
        f[i] = abs(f[i]) > 1.0e-6 ? f[i] : 0
    end

    Δx = -U * Diagonal(1.0 ./ h) * f
    self.modelEnergyChange = transpose(gradients)*Δx + 0.5*transpose(Δx)*hessian*Δx

    Δx
end
