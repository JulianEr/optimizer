# conda install -c conda-forge xtb
struct XtbEngine <: AbstractEngine
    path::String
    gfnMethod::Int64
end

XtbEngine(;path::String="xtb", gfnMethod::Int64=2) = XtbEngine(path, gfnMethod)

function makeCallAndReturnOutput(cmd::Cmd)
    out = Pipe()
    err = Pipe()
      
    run(pipeline(ignorestatus(cmd), stdout=out, stderr=err))
    close(out.in)
    close(err.in)

    error = String(read(err))
    if isempty(error)
        @error("Wrong! $error")
    end
      
    String(read(out))
end

function makeCalculation(self::XtbEngine, call::Function, symbols::Array{String}, coordinates::Array{Float64})
    dirName = mktempdir("."; prefix="xtb_", cleanup=false)
    root = pwd()
    cd(dirName)

    writeInputFile(self, symbols, coordinates)
    result = call()

    cd(root)
    rm(dirName, recursive=true)

    result
end

function getEnergyAndGradients(self::XtbEngine, symbols::Array{String}, coordinates::Array{Float64})
    function call()
        out = makeCallAndReturnOutput(`$(self.path) --grad --gfn $(self.gfnMethod) coords`)
        getEnergyFromFileXtb(), getGradientsFromFileAndRotationMatrixXtb(coordinates)[1]
    end

    seconds = @elapsed result = makeCalculation(self, call, symbols, coordinates)
    @info @sprintf("Took %.2f seconds to calculate energy and gradients", seconds)
    result
end

function getGradientsAndHessian(self::XtbEngine, symbols::Array{String}, coordinates::Array{Float64})
    function call()
        out = makeCallAndReturnOutput(`$(self.path) --hess --grad --gfn $(self.gfnMethod) coords`)
        parseGradientsAndHessian(self, length(coordinates)÷3)

        g, U = getGradientsFromFileAndRotationMatrixXtb(coordinates)[1]
        g, getHessianFromFileXtb(length(coordinates)÷3, U)
    end

    seconds = @elapsed result = makeCalculation(self, call, symbols, coordinates)
    @info @sprintf("Took %.2f seconds to calculate gradients and Hessian", seconds)
    result
end

function writeInputFile(::XtbEngine, symbols::Array{String}, coordinates::Array{Float64})
    open("coords", "w") do oFile
        write(oFile, "\$coord\n")
        for i ∈ 1:length(coordinates)÷3
            @printf(oFile, "\t%25.15f %25.15f %25.15f %s\n", coordinates[(i-1)*3 + 1], coordinates[(i-1)*3 + 2], coordinates[(i-1)*3 + 3], symbols[i])
        end
        write(oFile, "\$end\n")
    end
end

function getEnergyFromFileXtb()
    fileContent = open("energy") do iFile
        read(iFile, String)
    end
    pattern = r"[+-]?\d+\.\d+[+-]?[eE]?\d*";

    parse(Float64, match(pattern, fileContent).match)
end

function getGradientsAndRotationMatrix(input::String, coordinates::Array{Float64})
    numberOfAtoms = length(coordinates)÷3
    
    molecule = Array{Float64}(undef, numberOfAtoms, 3)
    pattern = r"(-?\d+\.\d+)\s*(-?\d+\.\d+)\s*(-?\d+\.\d+)\s*[A-Z][a-z]*"
    offset = 1

    for i ∈ 1:numberOfAtoms
        m = match(pattern, input, offset)
        
        offset = m.offsets[end]+1
        molecule[i, 1] = parse(Float64, m.captures[1])
        molecule[i, 2] = parse(Float64, m.captures[2])
        molecule[i, 3] = parse(Float64, m.captures[3])
    end

    coords2D = toBaryCenter(cartesianFlattenTo2d(copy(coordinates)))

    _, q = getBestFittingQuaternion2D(coords2D, molecule)

    gradients = Array{Float64}(undef, numberOfAtoms, 3)

    pattern = r"(-?\d\.\d+E[+-]\d{2})\s*(-?\d\.\d+E[+-]\d{2})\s*(-?\d\.\d+E[+-]\d{2})"
    
    for i ∈ 1:numberOfAtoms
        m = match(pattern, input, offset)
        offset = m.offsets[end]+2 # +2 because the first minus of the next match is optional. This sometimes skrews this over
        gradients[i, 1] = parse(Float64, m.captures[1])
        gradients[i, 2] = parse(Float64, m.captures[2])
        gradients[i, 3] = parse(Float64, m.captures[3])
    end

    @info("q_0 in gradient calculation to get gradient to current structure: $(q[1]) Center Of Mass for the gradients: $(getBaryCenter(gradients))")
    
    if abs(q[1] - 1) < 1e-8
        return Array{Float64}(reshape(gradients', length(gradients))), Diagonal(fill(1.0, 3))
    end
    U = getRotationMartixFromQuaternion(q)
    gradients =  gradients*U'
    
    Array{Float64}(reshape(gradients', length(gradients))), U
end

function getGradientsFromFileAndRotationMatrixXtb(coordinates::Array{Float64})
    
    fileContent = open("gradient") do iFile
        read(iFile, String)
    end
    getGradientsAndRotationMatrix(fileContent, coordinates)
end

function getHessianFromFileXtb(numberOfAtoms::Int64, U::AbstractArray{Float64, 2})
    fileContent = open("hessian") do iFile
        read(iFile, String)
    end
    n = numberOfAtoms*3
    H = Array{Float64}(undef, n, n)

    pattern = r"[+-]?\d+\.\d+"

    count = 0
    for m in eachmatch(pattern, fileContent)
        H[count÷n + 1, count%n + 1] = parse(Float64, m.match)
        count += 1
    end

    for i ∈ 1:size(H, 1)÷3
        for j ∈ 1:size(H, 2)÷3
            H[3*i-2:3*i, 3*j-2:3*j] = U*H[3*i-2:3*i, 3*j-2:3*j]*U'
        end
    end
    Array{Float64, 2}(H)
end
