struct AnalyticGradientsAndHessian <: AbstractGradientsAndHessian
    symbols::Array{String}
    engine::AbstractEngine
end

function getEnergyAndGradients(self::AnalyticGradientsAndHessian, optimizer::AbstractOptimizer)
    E, g = getEnergyAndGradients(self.engine, self.symbols, getCartesians(optimizer))
    E, transformCartesianGradients(optimizer, g)
end

function notify!(::AnalyticGradientsAndHessian, ::AbstractOptimizer) end

getHessian(self::AnalyticGradientsAndHessian, optimizer::AbstractOptimizer) = getAnalyticHessian(self, optimizer)

function getAnalyticHessian(self::AnalyticGradientsAndHessian, optimizer::AbstractOptimizer)
    g, H = getGradientsAndHessian(self.engine, self.symbols, getCartesians(optimizer))
    _, H_q = transformCartesianHessian(optimizer, H, g)
    Hermitian(H_q)
end
