function getBmatrix(internalCoordinates::Array{AbstractInternalCoordinate}, coordinates::Array{Float64})
    B = Array{Float64}(undef, (length(internalCoordinates), length(coordinates)))
    for i ∈ 1:length(internalCoordinates)
        B[i,:] = derivativeVector(internalCoordinates[i], coordinates)
    end
    B
end

function getGmatrix(B::Array{Float64, 2})
    B*B'
end

function getG⁻¹(B::Array{Float64, 2})
    G = getGmatrix(B)
    pinv(G, 1e-7)
end

# Remove duplicate Code
function getBG⁻¹(internalCoordinates::AbstractSetOfInternalCoordinates, cartesians::Array{Float64})
    B = getBmatrix(internalCoordinates, cartesians)
    B, getG⁻¹(B)
end

getRms(v::Array{Float64}) = sqrt(1.0/length(v)*dot(v,v))

getAbsMax(v::Array{Float64}) = maximum(abs.(v))