function numericGradientsAndHessian(internal::AbstractInternalCoordinate, xs::Array{Float64}, numericThreshold::Float64 = 1.0e-4)
    numericGradientsAndHessian((xs) -> value(internal, xs), xs, numericThreshold)
end

function numericGradientsAndHessian(f::Function, xs::Array{Float64}, numericThreshold::Float64 = 1.0e-4)
    f_i_j = f(xs)
    g = Array{Float64, 1}(undef, length(xs))
    H = Array{Float64, 2}(undef, (length(xs), length(xs)))
    calculatedValues = Array{Tuple{Float64, Float64}, 1}(undef, length(xs))
    for i ∈ 1:length(xs)
        xs[i] += numericThreshold
        f_ihp_j = f(xs)
        xs[i] -= 2numericThreshold
        f_ihm_j = f(xs)
        xs[i] += numericThreshold
        calculatedValues[i] = (f_ihp_j, f_ihm_j)
        g[i] = (f_ihp_j - f_ihm_j)/(2numericThreshold)
        H[i,i] = (f_ihp_j - 2f_i_j + f_ihm_j)/(numericThreshold^2)
    end
    for i ∈ 1:length(xs)
        for j ∈ i+1:length(xs)
            xs[[i;j]] .+= numericThreshold
            f_ihp_jhp = f(xs)
            xs[[i;j]] .-= 2numericThreshold
            f_ihm_jhm = f(xs)
            xs[[i;j]] .+= numericThreshold
            f_ihp_j, f_ihm_j = calculatedValues[i]
            f_i_jhp, f_i_jhm = calculatedValues[j]
            H[i,j] = (f_ihp_jhp - f_ihp_j - f_i_jhp + 2f_i_j - f_ihm_j - f_i_jhm + f_ihm_jhm)/(2*numericThreshold^2)
            H[j,i] = H[i,j]
        end
    end
    g, H
end

function numericGradientsAndHessianMultiValues(f::Function, xs::Array{Float64}, numericThreshold::Float64 = 1.0e-4)
    f_i_j = f(xs)
    g = Array{Float64, 2}(undef, length(f_i_j), length(xs))
    H = Array{Float64, 3}(undef, length(f_i_j), length(xs), length(xs))
    calculatedValues = Array{Tuple{Array{Float64}, Array{Float64}}, 1}(undef, length(xs))
    for i ∈ 1:length(xs)
        xs[i] += numericThreshold
        f_ihp_j = f(xs)
        xs[i] -= 2numericThreshold
        f_ihm_j = f(xs)
        xs[i] += numericThreshold
        calculatedValues[i] = (f_ihp_j, f_ihm_j)
        g[:, i] = (f_ihp_j - f_ihm_j)./(2numericThreshold)
        H[:, i, i] = (f_ihp_j - 2f_i_j + f_ihm_j)./(numericThreshold^2)
    end
    for i ∈ 1:length(xs)
        for j ∈ i+1:length(xs)
            xs[[i;j]] .+= numericThreshold
            f_ihp_jhp = f(xs)
            xs[[i;j]] .-= 2numericThreshold
            f_ihm_jhm = f(xs)
            xs[[i;j]] .+= numericThreshold
            f_ihp_j, f_ihm_j = calculatedValues[i]
            f_i_jhp, f_i_jhm = calculatedValues[j]
            H[:, i, j] = (f_ihp_jhp - f_ihp_j - f_i_jhp + 2f_i_j - f_ihm_j - f_i_jhm + f_ihm_jhm)./(2*numericThreshold^2)
            H[:, j, i] = H[:, i, j]
        end
    end
    g, H
end