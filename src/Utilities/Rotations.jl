function getCorrelationMatrix(oldCartesian::AbstractArray{Float64, 2}, newCartesians::AbstractArray{Float64, 2})
    return newCartesians'*oldCartesian
end

function getMatrixF(oldCartesian::AbstractArray{Float64, 2}, newCartesians::AbstractArray{Float64, 2})
    R = getCorrelationMatrix(oldCartesian, newCartesians)
    Hermitian([(R[1,1] + R[2,2] + R[3,3]) (R[2,3] - R[3,2])          ( R[3,1] - R[1,3])          ( R[1,2] - R[2,1]);
     (R[2,3] - R[3,2])          (R[1,1] - R[2,2] - R[3,3]) ( R[1,2] + R[2,1])          ( R[1,3] + R[3,1]);
     (R[3,1] - R[1,3])          (R[1,2] + R[2,1])          (-R[1,1] + R[2,2] - R[3,3]) ( R[2,3] + R[3,2]);
     (R[1,2] - R[2,1])          (R[1,3] + R[3,1])          ( R[2,3] + R[3,2])          (-R[1,1] - R[2,2] + R[3,3])])
end

function getBestFittingQuaternionAndF(oldCartesian::AbstractArray{Float64,2}, newCartesians::AbstractArray{Float64,2})
    F = getMatrixF(oldCartesian, newCartesians)
    @debug "F Matrix in getBestFittingQuaterionAndF:" F=F norm(diff(F))=norm(F-F')
    E, L = eigen(F)
    if L[1,4] < 0.0
        E[4], -L[:,4], F
    else
        E[4], L[:,4], F
    end
end

function getBestFittingQuaternion2D(oldCartesian::AbstractArray{Float64,2}, newCartesians::AbstractArray{Float64,2})
    E, L, _ = getBestFittingQuaternionAndF(oldCartesian, newCartesians)
    E, L
end

function getBestFittingQuaternion(oldCartesian::Array{Float64}, newCartesians::Array{Float64})
    old = toBaryCenter(cartesianFlattenTo2d(oldCartesian))
    new = toBaryCenter(cartesianFlattenTo2d(newCartesians))
    getBestFittingQuaternion2D(old, new)
end

function multiplyQuaternion(q1::Array{Float64}, q2::Array{Float64})
    [q1[1]*q2[1] - q1[2:end]'*q2[2:end]; q1[1]*q2[2:end] + q2[1]*q1[2:end] + cross(q1[2:end], q2[2:end])]
end 

function getRotationMartixFromQuaternion(q::Array{Float64})
    [(q[1]^2 + q[2]^2 - q[3]^2 - q[4]^2) (2*(q[2]*q[3] - q[1]*q[4]))         (2*(q[2]*q[4] + q[1]*q[3]));
     (2*(q[2]*q[3] + q[1]*q[4]))         (q[1]^2 - q[2]^2 + q[3]^2 - q[4]^2) (2*(q[3]*q[4] - q[1]*q[2]));
     (2*(q[2]*q[4] - q[1]*q[3]))         (2*(q[3]*q[4] + q[1]*q[2]))         (q[1]^2 - q[2]^2 - q[3]^2 + q[4]^2)]
end

function getRotationMatrixForMaximumCoincidence(oldCartesian::Array{Float64}, newCartesians::Array{Float64})
    getRotationMartixFromQuaternion(getBestFittingQuaternion(oldCartesian, newCartesians)[2])
end

function getQuaternionAndRMSD(oldCartesian::Array{Float64}, newCartesians::Array{Float64})
    old = toBaryCenter(cartesianFlattenTo2d(copy(oldCartesian)))
    new = toBaryCenter(cartesianFlattenTo2d(copy(newCartesians)))
    e, q = getBestFittingQuaternion2D(old, new)
    (q, sqrt(let x = sum(old.^2)+sum(new.^2) - 2.0*e
        abs(x) > 1.0e-10 ? x : 0.0
    end
    )/size(old, 1))
end

function getRmsdViaBestFittingQuaternion(oldCartesian::Array{Float64}, newCartesians::Array{Float64})
    _, rmsd = getQuaternionAndRMSD(oldCartesian, newCartesians)
    rmsd
end