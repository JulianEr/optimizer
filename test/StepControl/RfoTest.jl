function rsrfoTests()

    @testset "RFO Tests" begin
        @testset "Single RFO step" begin
            rsrfoStepControl = Tropic.RSRFO(;trustRadius=1.0)
            Δxs_rsrfo = Tropic.takeStep!(rsrfoStepControl, OptimizerRfoDouble())
            @test norm(Δxs_rsrfo - h2o2twiceinternalChange) < 1e-6
            
            scalingRFOStepControl = Tropic.ScalingRFO(trustRadius=1.0)
            Δxs_scalingRfo = Tropic.takeStep!(scalingRFOStepControl, OptimizerRfoDouble())
            @test norm(Δxs_scalingRfo - h2o2twiceinternalChange) < 1e-6

            simpleRFOStepControl = Tropic.SimpleRFO()
            Δxs_simpleRfo = Tropic.takeStep!(simpleRFOStepControl, OptimizerRfoDouble())
            @test norm(Δxs_simpleRfo - h2o2twiceinternalChange) < 1e-6
        end

        @testset "Single Scaled RFO step" begin
            rsrfoStepControl = Tropic.RSRFO(trustRadius=0.3, ε₁=0.01)
            Δxs_rsrfo = Tropic.takeStep!(rsrfoStepControl, OptimizerRfoDouble()) 
            @test norm(Δxs_rsrfo - h2o2twiceScaledInternalChangeRsrfo) < 1e-6
            @test abs(norm(Δxs_rsrfo) - 0.3) < 1e-6
            
            scalingRFOStepControl = Tropic.ScalingRFO(trustRadius=0.3)
            Δxs_scalingRfo = Tropic.takeStep!(scalingRFOStepControl, OptimizerRfoDouble())
            @test norm(Δxs_scalingRfo - h2o2twiceScaledInternalChangeScalingRfo) < 1e-6
            @test abs(norm(Δxs_scalingRfo) - 0.3) < 1e-6
        end
    end

end

