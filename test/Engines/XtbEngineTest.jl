function xtbEngineTest()
    @testset "Xtb Engine Tests" begin
        @testset "Read in gradient content" begin
            g, U = Tropic.getGradientsAndRotationMatrix(gradientContent_EnginesTestData, cartesians_EnginesTestData)
            
            @test norm(U-I) < 1e-7
            @test norm(g - expectedRedInGradients_EnginesTestData) < 1e-7
        end
    end
end