include("DelocalizedInternalCoordinatesTestData.jl")

function delocalizedInternalCoordinatesTest()
    @testset "Delocalized Internal Coordinates Tests" begin
        @testset "Delocalized Internal Coordinates Construction Tests" begin
            internalCoordinates = Tropic.DelocalizedInternalCoordinates(h2o2TwiceInternalCoordinates_DelocalizedInternalCoordiantesTestData, h2o2TwiceCartesians_DelocalizedInternalCoordiantesTestData)
            internalCoordinatesNoConstraints = Tropic.DelocalizedInternalCoordinates(h2o2TwiceInternalCoordinates_DelocalizedInternalCoordiantesTestData, h2o2TwiceCartesians_DelocalizedInternalCoordiantesTestData, constraintOverallTR=false)
            
            @test norm(internalCoordinates.U'*internalCoordinates.U - I) < 1e-7
            @test norm(internalCoordinates.U*internalCoordinates.U' - I) > 1e-7
            @test sum([norm(c) for c in eachcol(internalCoordinates.U)]) - 18 < 1e-7

            @test norm(internalCoordinatesNoConstraints.U'*internalCoordinatesNoConstraints.U - I) < 1e-7
            @test norm(internalCoordinatesNoConstraints.U*internalCoordinatesNoConstraints.U' - I) > 1e-7
            @test sum([norm(c) for c in eachcol(internalCoordinatesNoConstraints.U)]) - 24 < 1e-7


            formaicAcidInternalCoordinates = Tropic.DelocalizedInternalCoordinates(formicAcidPrimitives_DelocalizedInternalCoordiantesTestData, formicAcidCartesians_DelocalizedInternalCoordiantesTestData; constraints=formicAcidConstraints_DelocalizedInternalCoordiantesTestData)
            @test norm(formaicAcidInternalCoordinates.U'*formaicAcidInternalCoordinates.U - I) < 1e-7
            @test norm(formaicAcidInternalCoordinates.U*formaicAcidInternalCoordinates.U' - I) > 1e-7
            @test norm(formaicAcidInternalCoordinates.U[1,:]) - 1 < 1e-7
            @test norm(formaicAcidInternalCoordinates.U[2,:]) - 1 < 1e-7
            @test norm(formaicAcidInternalCoordinates.U[3,:]) - 1 < 1e-7
            @test norm(formaicAcidInternalCoordinates.U[4,:]) - 1 < 1e-7
            @test norm(formaicAcidInternalCoordinates.U[5,:]) - 1 < 1e-7
            @test norm(formaicAcidInternalCoordinates.U[6,:]) < 1e-7
            @test norm(formaicAcidInternalCoordinates.U[7,:]) < 1e-7
            @test norm(formaicAcidInternalCoordinates.U[8,:]) < 1e-7
            @test norm(formaicAcidInternalCoordinates.U[9,:]) - 1 < 1e-7
            @test norm(formaicAcidInternalCoordinates.U[10,:]) - 1 < 1e-7
            @test sum([norm(c) for c in eachcol(formaicAcidInternalCoordinates.U)]) - 7 < 1e-7
        end

        # @testset "Delocalized Internal Coordinates Set and Apply new Coordinates Tests" begin
        # end

        # @testset "Delocalized Internal Coordinates Change and Values Tests" begin
        # end

        # @testset "Gradients and Hessian for Delocalized Internal Coordinates Tests" begin
        # end

        # @testset "Hessian Guesses for Delocalized Internal Coordinates Tests" begin
        # end

        # @testset "Hessian Guesses for Delocalized Internal Coordinates Tests" begin
        # end
    end
end