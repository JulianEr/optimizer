function defaultLineTest()
    @testset "Default Line Tests" begin
        defaultLine = Tropic.DefaultLine(4, Tropic.Bond(3,4), Tropic.Angle(2,3,4), Tropic.Dihedral(1,2,3,4))
        @testset "Internal Values for Default Line Test" begin
            @test norm(Tropic.getInternalValues(defaultLine, h2o2Cartesians_LinesTestData) - expectedValuesForDefaultLine_LinesTestData) < 1e-7
        end

        @testset "B-Matrix Line for Default Line Test" begin
            @test norm(Tropic.getBmatrixLines(defaultLine, h2o2Cartesians_LinesTestData) - expectedBmatrixLineForDefaultLine_LinesTestData) < 1e-7
        end

        @testset "B-Matrix Derivatives for Default Line Test" begin
            @test norm(Tropic.getDerivativeOfBmatrixMatrices(defaultLine, h2o2Cartesians_LinesTestData) - expectedBmatrixDerivativesForDefaultLine_LinesTestData) < 1e-7
        end

        @testset "Hessian Guess for Default Line Test" begin
            @test norm(Tropic.getHessianGuessValues(defaultLine, h2o2Symbols_LinesTestData, h2o2Cartesians_LinesTestData) - expectedHessianGuessForDefaultLine_LinesTestData) < 1e-7
        end

        @testset "Schlegel Hessian Guess for Default Line Test" begin
            @test norm(Tropic.getHessianGuessSchlegelValues(defaultLine, h2o2Symbols_LinesTestData, h2o2Cartesians_LinesTestData) - expectedSchlegelHessianGuessForDefaultLine_LinesTestData) < 1e-7
        end

        @testset "Print Default Line Test" begin
            @test Tropic.printLine(defaultLine, h2o2Symbols_LinesTestData, allInternalValues_LinesTestData) == expectedPrintedDefaultLine_LinesTestData
        end

        @testset "Add to Cartesian for Default Line Test" begin
            cartesians = Array{Float64}(undef, 12)
            cartesians[1:9] = h2o2Cartesians_LinesTestData[1:9]

            Tropic.addToCartesian!(defaultLine, allInternalValues_LinesTestData, cartesians)

            @test norm(cartesians - h2o2Cartesians_LinesTestData) < 1e-7
        end
    end
end