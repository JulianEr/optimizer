function rotationLineTest()
    @testset "Rotation Line Tests" begin
        rotationLine = Tropic.RotationLine(
            Tropic.RotationA([1, 2, 3, 4], h2o2Cartesians_LinesTestData),
            Tropic.RotationB([1, 2, 3, 4], h2o2Cartesians_LinesTestData),
            Tropic.RotationC([1, 2, 3, 4], h2o2Cartesians_LinesTestData)
        )
        @testset "Internal Values for Rotation Line Test" begin
            a, b, c = Tropic.getInternalValues(rotationLine, rotatedAndShiftedH2o2Cartesians_LinesTestData)
            expectedA, expectedB, expectedC = expectedValuesForRotationLine_LinesTestData
            @test isapprox(a, expectedA)
            @test isapprox(b, expectedB)
            @test isapprox(c, expectedC)
        end

        @testset "B-Matrix Line for Rotation Line Test" begin
            @test norm(Tropic.getBmatrixLines(rotationLine, rotatedAndShiftedH2o2Cartesians_LinesTestData) - expectedBmatrixLineForRotationLine_LinesTestData) < 1e-7
        end

        @testset "B-Matrix Derivatives for Rotation Line Test" begin
            @test norm(Tropic.getDerivativeOfBmatrixMatrices(rotationLine, rotatedAndShiftedH2o2Cartesians_LinesTestData) - expectedBmatrixDerivativesForRotationLine_LinesTestData) < 1e-7
        end

        @testset "Hessian Guess for Rotation Line Test" begin
            @test norm(Tropic.getHessianGuessValues(rotationLine, h2o2Symbols_LinesTestData, rotatedAndShiftedH2o2Cartesians_LinesTestData) - expectedHessianGuessForRotationLine_LinesTestData) < 1e-7
        end

        @testset "Schlegel Hessian Guess for Rotation Line Test" begin
            @test norm(Tropic.getHessianGuessSchlegelValues(rotationLine, h2o2Symbols_LinesTestData, rotatedAndShiftedH2o2Cartesians_LinesTestData) - expectedSchlegelHessianGuessForRotationLine_LinesTestData) < 1e-7
        end

        @testset "Print Rotation Line Test" begin
            @test Tropic.printLine(rotationLine, expectedValuesForRotationLine_LinesTestData) == "Rotation     -77.7095979  258.9304047  -95.2487254"
        end
    end
end