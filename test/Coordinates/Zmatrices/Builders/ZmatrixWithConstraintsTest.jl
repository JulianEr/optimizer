function zmatrixWithConstraintsTest()
    @testset "Z-Matrix with Constraints Tests" begin
        @testset "Get Specific Bond Lines Test" begin
            bondDict = Dict(2=>Tropic.Bond(1,2), 3=>Tropic.Bond(2,3), 5=>Tropic.Bond(4,5))
            constraints = Int64[]
            foundBondLines = Tropic.getBondLines!(bondDict, constraints)
            sort!(foundBondLines, by=first)
            @test foundBondLines[1] == (2, Tropic.Bond(1,2))
            @test foundBondLines[2] == (3, Tropic.Bond(2,3))
            @test foundBondLines[3] == (5, Tropic.Bond(4,5))
            sort!(constraints)
            @test constraints == [1, 2, 7]
        end

        @testset "Get Specific Angle Lines Test" begin
            bondDict = Dict(6=>Tropic.Bond(5,6))
            angleDict = Dict(6=>Tropic.Angle(2,1,6))
            constraints = Int64[]

            @test_throws ErrorException Tropic.getAngleLines!(angleDict, bondDict, constraints)

            bondDict = Dict(3=>Tropic.Bond(2,3), 6=>Tropic.Bond(5,6))
            angleDict = Dict(3=>Tropic.Angle(1,2,3), 6=>Tropic.Angle(4,5,6), 8=>Tropic.Angle(6,7,8))

            foundAngleLines = Tropic.getAngleLines!(angleDict, bondDict, constraints)
            sort!(foundAngleLines, by=first)
        
            (n, (b,a)) = foundAngleLines[1]
            @test n == 3
            @test b == Tropic.Bond(2,3)
            @test a == Tropic.Angle(1,2,3)
            (n, (b,a)) = foundAngleLines[2]
            @test n == 6
            @test b == Tropic.Bond(5,6)
            @test a == Tropic.Angle(4,5,6)
            (n, (b,a)) = foundAngleLines[3]
            @test n == 8
            @test b == Tropic.Bond(7,8)
            @test a == Tropic.Angle(6,7,8)

            sort!(constraints)
            @test constraints == [2, 3, 10, 11, 17]
        end

        @testset "Get Specific Dihedral Lines Test" begin
            bondDict = Dict(6=>Tropic.Bond(5,6))
            angleDict = Dict(6=>Tropic.Angle(2,1,6))
            dihedralDict = Dict(6=>Tropic.Dihedral(3,4,5,6))
            constraints = Int64[]

            @test_throws ErrorException Tropic.getDihedralLines!(dihedralDict, angleDict, bondDict, constraints)
            
            bondDict = Dict(6=>Tropic.Bond(5,6))
            angleDict = Dict(6=>Tropic.Angle(2,1,6))
            dihedralDict = Dict(6=>Tropic.Dihedral(3,2,1,6))

            @test_throws ErrorException Tropic.getDihedralLines!(dihedralDict, angleDict, bondDict, constraints)
            
            bondDict = Dict(4=>Tropic.Bond(3,4),6=>Tropic.Bond(5,6))
            angleDict = Dict(4=>Tropic.Angle(2,3,4),5=>Tropic.Angle(3,4,5))
            dihedralDict = Dict(4=>Tropic.Dihedral(1,2,3,4),5=>Tropic.Dihedral(2,3,4,5),6=>Tropic.Dihedral(3,4,5,6),7=>Tropic.Dihedral(4,5,6,7))

            foundDihedralLines = Tropic.getDihedralLines!(dihedralDict, angleDict, bondDict, constraints)
            sort!(foundDihedralLines, by=first)

            (n, (b,a,d)) = foundDihedralLines[1]
            @test n == 4
            @test b == Tropic.Bond(3,4)
            @test a == Tropic.Angle(2,3,4)
            @test d == Tropic.Dihedral(1,2,3,4)
            (n, (b,a,d)) = foundDihedralLines[2]
            @test n == 5
            @test b == Tropic.Bond(4,5)
            @test a == Tropic.Angle(3,4,5)
            @test d == Tropic.Dihedral(2,3,4,5)
            (n, (b,a,d)) = foundDihedralLines[3]
            @test n == 6
            @test b == Tropic.Bond(5,6)
            @test a == Tropic.Angle(4,5,6)
            @test d == Tropic.Dihedral(3,4,5,6)
            (n, (b,a,d)) = foundDihedralLines[4]
            @test n == 7
            @test b == Tropic.Bond(6,7)
            @test a == Tropic.Angle(5,6,7)
            @test d == Tropic.Dihedral(4,5,6,7)

            sort!(constraints)
            @test constraints == [4, 5, 6, 8, 9, 10, 11, 12, 15]
        end

        @testset "Find Matching Internal Coordinates Tests" begin
            bond = Tropic.Bond(4,5)
            angles = Tropic.Angle[Tropic.Angle(1,2,4), Tropic.Angle(3,4,5), Tropic.Angle(1,2,4), Tropic.Angle(5,4,1)]
            foundAngles = Tropic.findAnglesMatchingBond(bond, angles)
            @test length(foundAngles) == 2
            @test foundAngles[1] == Tropic.Angle(3,4,5)
            @test foundAngles[2] == Tropic.Angle(1,4,5)

            angle = Tropic.Angle(3,4,5)
            dihedrals = Tropic.Dihedral[Tropic.Dihedral(1,3,5,6), Tropic.Dihedral(1,3,4,5)]
            @test Tropic.findDihedralMatchingAngle(angle, dihedrals) == Tropic.Dihedral(1,3,4,5) 
            
            dihedrals = Tropic.Dihedral[Tropic.Dihedral(1,3,5,6), Tropic.Dihedral(5,4,3,1)]
            @test Tropic.findDihedralMatchingAngle(angle, dihedrals) == Tropic.Dihedral(5,4,3,1) 

            dihedrals = Tropic.Dihedral[Tropic.Dihedral(1,3,5,6), Tropic.Dihedral(1,2,3,4)]
            @test isnothing(Tropic.findDihedralMatchingAngle(angle, dihedrals))
        end

        @testset "Fill Angle Lines Test" begin
            filledLines = Tropic.fillAngleLines(someAngleLines_ZmatrixBuildersTest, Tropic.Dihedral[Tropic.Dihedral(1,2,3,4)])

            (n, (b,a,d)) = filledLines[1]
            @test n == 3
            @test b == Tropic.Bond(2,3)
            @test a == Tropic.Angle(1,2,3)
            @test d == Tropic.EmptyInternalCoordinate()
            (n, (b,a,d)) = filledLines[2]
            @test n == 4
            @test b == Tropic.Bond(3,4)
            @test a == Tropic.Angle(2,3,4)
            @test d == Tropic.Dihedral(1,2,3,4)

            @test_throws ErrorException Tropic.fillAngleLines(someAngleLines_ZmatrixBuildersTest, Tropic.Dihedral[])
        end

        @testset "Fill Bond Lines Test" begin
            filledLines = Tropic.fillBondLines(someBondLines_ZmatrixBuildersTest, Tropic.Dihedral[Tropic.Dihedral(2,3,4,5)], Tropic.Angle[Tropic.Angle(1,2,3), Tropic.Angle(3,4,5)])

            (n, (b,a,d)) = filledLines[1]
            @test n == 2
            @test b == Tropic.Bond(1,2)
            @test a == Tropic.EmptyInternalCoordinate()
            @test d == Tropic.EmptyInternalCoordinate()
            (n, (b,a,d)) = filledLines[2]
            @test n == 3
            @test b == Tropic.Bond(2,3)
            @test a == Tropic.Angle(1,2,3)
            @test d == Tropic.EmptyInternalCoordinate()
            (n, (b,a,d)) = filledLines[3]
            @test n == 5
            @test b == Tropic.Bond(4,5)
            @test a == Tropic.Angle(3,4,5)
            @test d == Tropic.Dihedral(2,3,4,5)

            @test_throws ErrorException Tropic.fillBondLines(someOtherBondLines_ZmatrixBuildersTest, Tropic.Dihedral[], Tropic.Angle[])
            @test_throws ErrorException Tropic.fillBondLines(evenMoreBondLines_ZmatrixBuildersTest, Tropic.Dihedral[], Tropic.Angle[])
            @test_throws ErrorException Tropic.fillBondLines(evenMoreBondLines_ZmatrixBuildersTest, Tropic.Dihedral[], Tropic.Angle[Tropic.Angle(3,4,5)])
        end

        @testset "zMatrixFromInternals Tests" begin
            constraintZmat, constraints = Tropic.zMatrixFromInternals(constrainablePrimitiveSystem_ZmatrixBuildersTest, constraintsForPrimitiveSystem_ZmatrixBuildersTest)
            @test constraints == [1, 3, 18, 23]
            _, constraints = Tropic.zMatrixFromInternals(constrainablePrimitiveSystem_ZmatrixBuildersTest, constraintsForPrimitiveSystemReverted_ZmatrixBuildersTest)
            @test constraints == [1, 3, 18, 23]
            _, constraints = Tropic.zMatrixFromInternals(constrainablePrimitiveSystem_ZmatrixBuildersTest, constraintsForPrimitiveSystemSameLine_ZmatrixBuildersTest)
            @test constraints == [4,5,6]
            _, constraints = Tropic.zMatrixFromInternals(constrainablePrimitiveSystem_ZmatrixBuildersTest, constraintsForPrimitiveSystemMoreInOneLine_ZmatrixBuildersTest)
            @test constraints == [4, 5, 6, 7, 9, 10, 11, 14, 15]
            zMat, constraints = Tropic.zMatrixFromInternals(constrainablePrimitiveSystem_ZmatrixBuildersTest)
            @test constraintZmat.lines[8].dihedral != zMat.lines[8].dihedral

            @test_throws ErrorException Tropic.zMatrixFromInternals(constrainablePrimitiveSystem_ZmatrixBuildersTest, constraintsForPrimitiveSystemSameCoordinateTwiceAngle_ZmatrixBuildersTest)
            @test_throws ErrorException Tropic.zMatrixFromInternals(constrainablePrimitiveSystem_ZmatrixBuildersTest, constraintsForPrimitiveSystemSameCoordinateTwiceDihedral_ZmatrixBuildersTest)
        end
    end
end