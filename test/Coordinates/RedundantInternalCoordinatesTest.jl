include("RedundantInternalCoordinatesTestData.jl")

function redundantInternalCoodinatesTest()
    @testset "Redundant Internal Coordinates Tests" begin
        primitives = Tropic.findAllInternalCoordinates(Tropic.buildBondGraph(h2o2TwiceSymbols_RedundantInternalCoordiantesTestData, h2o2TwiceCartesians_RedundantInternalCoordiantesTestData), h2o2TwiceSymbols_RedundantInternalCoordiantesTestData, h2o2TwiceCartesians_RedundantInternalCoordiantesTestData)
        internalCoordinates = Tropic.RedundantInternalCoordinates(primitives, h2o2TwiceCartesians_RedundantInternalCoordiantesTestData)
        @testset "Gradient Transformation" begin
            g = Tropic.transformCartesianGradients(internalCoordinates, h2o2TwiceCartesianGradients_RedundantInternalCoordiantesTestData)
            gₐ = Tropic.gradientsToActiveSpace(internalCoordinates, g)
            
            @test norm(g - h2o2twiceRedundantInternalGradients_RedundantInternalCoordiantesTestData) < 1e-6
            #@test norm(g - h2o2twiceNumericInternalGradients) < 1e-6
            @test norm(gₐ - h2o2twiceActiveRedundantInternalGradients_RedundantInternalCoordiantesTestData) < 1e-6
        end

        @testset "Hessian Transformation" begin
            _, H = Tropic.transformCartesianHessian(internalCoordinates, h2o2TwiceCartesianHessian_RedundantInternalCoordiantesTestData, h2o2TwiceCartesianGradients_RedundantInternalCoordiantesTestData, h2o2TwiceCartesians_RedundantInternalCoordiantesTestData)
            Hₐ = Tropic.hessianToActiveSpace(internalCoordinates, H)
            Hₐ_weights = Tropic.weightsToHessian(internalCoordinates, Hₐ)

            @test norm(H - h2o2twiceRedundantInternalHessian_RedundantInternalCoordiantesTestData) < 1e-6
            #@test norm(H - h2o2twichNumericInternalHessian) < 1e-3
            @test norm(Hₐ - h2o2twiceActiveRedundantInternalHessian_RedundantInternalCoordiantesTestData) < 1e-6
            @test norm(Hₐ_weights - h2o2twiceActiveRedundantInternalHessianWithWeights_RedundantInternalCoordiantesTestData) < 1e-6        
        end

        @testset "Benzene as redundant system" begin
            benzenePrimitives = Tropic.findAllInternalCoordinates(Tropic.buildBondGraph(benzeneSymbols_RedundantInternalCoordiantesTestData, benzeneCartesians_RedundantInternalCoordiantesTestData), benzeneSymbols_RedundantInternalCoordiantesTestData, benzeneCartesians_RedundantInternalCoordiantesTestData)
            benzeneInternals = Tropic.RedundantInternalCoordinates(benzenePrimitives, benzeneCartesians_RedundantInternalCoordiantesTestData)
            
            g, H = Tropic.transformCartesianHessian(benzeneInternals, benzeneCartesianHessian_RedundantInternalCoordiantesTestData, benzeneCartesianGradients_RedundantInternalCoordiantesTestData, benzeneCartesians_RedundantInternalCoordiantesTestData)

            gₐ = Tropic.gradientsToActiveSpace(benzeneInternals, g)
            Hₐ = Tropic.hessianToActiveSpace(benzeneInternals, H)
            Hₐ_weigths = Tropic.weightsToHessian(benzeneInternals, Hₐ)

            @test norm(g - benzeneInternalGradients_RedundantInternalCoordiantesTestData) < 1e-6
            @test norm(H - benzeneInternalHessina_RedundantInternalCoordiantesTestData) < 1e-6

            @test norm(gₐ - benzeneActiveInternalGradients_RedundantInternalCoordiantesTestData) < 1e-6
            @test norm(Hₐ - benzeneActiveInternalHessian_RedundantInternalCoordiantesTestData) < 1e-6
            @test norm(Hₐ_weigths - benzeneActiveInternalHessianWithWeights_RedundantInternalCoordiantesTestData) < 1e-6
        end
    end
end