function translationRotationAndPrimitivesTest()
    @testset "Testing Sets Of Internal Coordinates" begin

        @testset "Test if correct underlying system is built" begin 
            primitive = Tropic.TranslationRotationAndPrimitives([], [], [], [], [], [], [], primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest)

            @test isUnderlyingZmat_SetsOfInternalCoordinatesTest(Tropic.coordsForRedundantInternalCoordinates(primitive, h2o2Cartesians_SetsOfInternalCoordinatesTest))
            @test isUnderlyingZmat_SetsOfInternalCoordinatesTest(Tropic.coordsForRedundantInternalCoordinates(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest))
        end
        
        @testset "Creating B-Matrices" begin
            @test norm(Tropic.getBmatrix(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedTRBmatrix_SetsOfInternalCoordinatesTest) <= 1e-6

            emptyTrCoordinates = Tropic.TranslationRotationAndPrimitives([],[],[],[],[],[],[], translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest.primitives)
            @test norm(Tropic.getBmatrix(emptyTrCoordinates, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedBmatrix_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Creating second Derivative B-Matrices" begin
            @test norm(Tropic.getDerivativeOfBmatrix(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedTRBmatrixDerivatives_SetsOfInternalCoordinatesTest) <= 1e-6
            
            emptyTrCoordinates = Tropic.TranslationRotationAndPrimitives([],[],[],[],[],[],[], translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest.primitives)
            @test norm(Tropic.getDerivativeOfBmatrix(emptyTrCoordinates, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedBmatrixDerivatives_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Creating Hessian Guess" begin
            @test norm(Tropic.guessHessianLindh(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest, h2o2twiceSymbols_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedTRhessianGuess_SetsOfInternalCoordinatesTest) <= 1e-6
            
            emptyTrCoordinates = Tropic.TranslationRotationAndPrimitives([],[],[],[],[],[],[], translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest.primitives)
            @test norm(Tropic.guessHessianLindh(emptyTrCoordinates, h2o2twiceSymbols_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedHessianGuess_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Creating Hessian Guess Schlegel" begin
            @test norm(Tropic.guessHessianSchlegel(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest, h2o2twiceSymbols_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedTRhessianGuessSchlegel_SetsOfInternalCoordinatesTest) <= 1e-6
            
            emptyTrCoordinates = Tropic.TranslationRotationAndPrimitives([],[],[],[],[],[],[], translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest.primitives)
            @test norm(Tropic.guessHessianSchlegel(emptyTrCoordinates, h2o2twiceSymbols_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedHessianGuessSchlegel_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Remove Overwind" begin
            newTrValues = Tropic.removeOverwind(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest, trValuesOverwind_SetsOfInternalCoordinatesTest)
            @test norm(newTrValues - [0; 0; 0; 0; 0; 0; -0.5π; 0.5π; -π; π; 0.5π; -0.5π; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0.5π; -0.5π])  <= 1e-6

            emptyTrCoordinates = Tropic.TranslationRotationAndPrimitives([],[],[],[],[],[],[], translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest.primitives)
            newPrimitiveValuesEmptyTr = Tropic.removeOverwind(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest.primitives, primitiveValuesOverwind_SetsOfInternalCoordinatesTest)
            @test norm(newPrimitiveValuesEmptyTr - [0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0.5π; -0.5π])  <= 1e-6
        end

        @testset "Get all Values for internal cooridnate system" begin
            @test norm(Tropic.getValuesFromCartesians(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest, h2o2twiceRotatedCartesians_SetsOfInternalCoordinatesTest) - expectedTrValues_SetsOfInternalCoordinatesTest) <= 1e-6
            
            emptyTrCoordinates = Tropic.TranslationRotationAndPrimitives([],[],[],[],[],[],[], translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest.primitives)
            @test norm(Tropic.getValuesFromCartesians(emptyTrCoordinates, h2o2twiceRotatedCartesians_SetsOfInternalCoordinatesTest) - expectedPrimitiveValues_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Get Differences for internal coordinate system" begin
            
            @test norm(Tropic.getDifferences(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest, trValuesOverwindBefore_SetsOfInternalCoordinatesTest, trValuesOverwindAfter_SetsOfInternalCoordinatesTest) - [-0.1000000000;   0.1000000000;  -0.1000000000;   0.1000000000;  -0.1000000000;   0.1000000000;  -0.0314159265;   0.0000000000;   0.0628318531;  -0.0628318531;   0.0314159265;   0.0314159265;   0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;  -0.1000000000;  -0.1000000000;  -0.1000000000;  -0.1000000000;   0.0628318531;  -0.0628318531]) <= 1e-6
            
            emptyTrCoordinates = Tropic.TranslationRotationAndPrimitives([],[],[],[],[],[],[], translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest.primitives)
            @test norm(Tropic.getDifferences(emptyTrCoordinates, primitiveValuesOverwindBefore_SetsOfInternalCoordinatesTest, primitiveValuesOverwindAfter_SetsOfInternalCoordinatesTest) - [0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;  -0.1000000000;  -0.1000000000;  -0.1000000000;  -0.1000000000;   0.0628318531;  -0.0628318531]) <= 1e-6
        end

        @testset "Add Constraints to the set of internal coordinates" begin
            internalCoordinatesCopy = deepcopy(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest)
            primitiveConstraints = Tropic.PrimitiveConstraints([Tropic.BondConstraint(3, 8) Tropic.BondConstraint(2, 3) Tropic.BondConstraint(1, 4)], [Tropic.AngleConstraint(4, 3, 2) Tropic.AngleConstraint(5, 7, 8)], [Tropic.DihedralConstraint(5, 6, 7, 8) Tropic.DihedralConstraint(3, 4, 5, 6)])
            constraints = Tropic.TrAndPrimitiveConstraints(
                [Tropic.TranslationXConstraint([1, 2, 3, 4]), Tropic.TranslationXConstraint([1, 2, 3, 4, 5, 6, 7, 8])], 
                [Tropic.TranslationYConstraint([1, 2, 3, 4]), Tropic.TranslationYConstraint([1, 2, 3, 4, 5, 6, 7, 8])],
                [Tropic.TranslationZConstraint([1, 2, 3, 4]), Tropic.TranslationZConstraint([1, 2, 3, 4, 5, 6, 7, 8])],
                [Tropic.RotationAConstraint([5, 6, 7, 8]), Tropic.RotationAConstraint([1, 2, 3, 4, 5, 6, 7, 8])],
                [Tropic.RotationBConstraint([5, 6, 7, 8]), Tropic.RotationBConstraint([1, 2, 3, 4, 5, 6, 7, 8])],
                [Tropic.RotationCConstraint([5, 6, 7, 8]), Tropic.RotationCConstraint([1, 2, 3, 4, 5, 6, 7, 8])],
                primitiveConstraints
            )
            _, listOfConstraints = Tropic.addAndFindConstraints!(internalCoordinatesCopy, constraints, h2o2twiceRotatedCartesians_SetsOfInternalCoordinatesTest)
            expectedListOfConstraints = [1, 3, 4, 6, 7, 9, 11, 12, 14, 15, 17, 18, 25, 20, 26, 28, 31, 33, 34]
            @test listOfConstraints == expectedListOfConstraints
            @test length(internalCoordinatesCopy.translationsX) == 3
            @test length(internalCoordinatesCopy.translationsY) == 3
            @test length(internalCoordinatesCopy.translationsZ) == 3
            @test length(internalCoordinatesCopy.rotationsA) == 3
            @test length(internalCoordinatesCopy.rotationsB) == 3
            @test length(internalCoordinatesCopy.rotationsC) == 3
            @test length(internalCoordinatesCopy.primitives.bonds) == 8
            @test length(internalCoordinatesCopy.primitives.angles) == 5
            @test length(internalCoordinatesCopy.primitives.dihedrals) == 3

            expectedListOfConstraintsForPrimitivesToTr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 19, 14, 20, 22, 25, 27, 28]
            primitiveInternalCoordinates = deepcopy(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest.primitives)
            _, listOfConstraintsForPrimitivesToTr = Tropic.addAndFindConstraints!(primitiveInternalCoordinates, constraints, h2o2twiceRotatedCartesians_SetsOfInternalCoordinatesTest)
            @test expectedListOfConstraintsForPrimitivesToTr == listOfConstraintsForPrimitivesToTr
        
            primitiveInternalCoordinates = deepcopy(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest.primitives)
            Tropic.addAndFindConstraints!(primitiveInternalCoordinates, constraints, h2o2twiceRotatedCartesians_SetsOfInternalCoordinatesTest)
        end

        @testset "Do Translation And Rotation Internal Coordinates have Translation And Rotation Tests" begin
            @test Tropic.hasTranslationAndRotation(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest, false) == true
            @test Tropic.hasTranslationAndRotation(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest, true) == true
        end

        @testset "Test printing of internal coordinate set" begin
            expectedOutput = """TranslationX(1, 2, 3, 4) -10.5555377053
                                TranslationX(5, 6, 7, 8)  -6.0898990000
                                TranslationY(1, 2, 3, 4)   0.3560716452
                                TranslationY(5, 6, 7, 8)  -1.4151415000
                                TranslationZ(1, 2, 3, 4)   0.5520362444
                                TranslationZ(5, 6, 7, 8)  -0.8864480000
                                RotationA(1, 2, 3, 4)   0.0000000000
                                RotationA(5, 6, 7, 8)  -0.4526489239
                                RotationB(1, 2, 3, 4)   0.0000000000
                                RotationB(5, 6, 7, 8)  -0.6230004263
                                RotationC(1, 2, 3, 4)   0.0000000000
                                RotationC(5, 6, 7, 8)   0.0356125220
                                Bond(1, 2)   1.9161483355
                                Bond(2, 3)   2.8502673624
                                Bond(3, 4)   1.9179248737
                                Bond(5, 6)   1.8391006826
                                Bond(6, 7)   2.7743353175
                                Bond(7, 8)   1.8719814430
                                Angle(1, 2, 3)   1.8995648767
                                Angle(2, 3, 4)   1.9089816985
                                Angle(5, 6, 7)   1.8524852504
                                Angle(6, 7, 8)   1.7314345129
                                Dihedral(1, 2, 3, 4)  -2.5730428532
                                Dihedral(5, 6, 7, 8)  -1.6225437438
                                """
            @test Tropic.toString(translationRotationAndPrimitiveSystem_SetsOfInternalCoordinatesTest, expectedTrValues_SetsOfInternalCoordinatesTest) == expectedOutput
        end
    end
end