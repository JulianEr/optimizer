function primitivesTest()
    @testset "Testing Sets Of Internal Coordinates" begin

        @testset "Test if correct underlying system is built" begin 
            @test isUnderlyingZmat_SetsOfInternalCoordinatesTest(Tropic.coordsForRedundantInternalCoordinates(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, h2o2Cartesians_SetsOfInternalCoordinatesTest))
            @test !isUnderlyingZmat_SetsOfInternalCoordinatesTest(Tropic.coordsForRedundantInternalCoordinates(ammoniaPrimitiveSystem_SetsOfInternalCoordinatesTest, ammoniaCartesians_SetsOfInternalCoordinatesTest))
        end

        @testset "Creating B-Matrices" begin
            @test norm(Tropic.getBmatrix(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, h2o2Cartesians_SetsOfInternalCoordinatesTest) - expectedPrimitiveBmatrix_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Creating second Derivative B-Matrices" begin
            @test norm(Tropic.getDerivativeOfBmatrix(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, h2o2Cartesians_SetsOfInternalCoordinatesTest) - expectedPrimitiveBmatrixDerivatives_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Creating Hessian Guess" begin
            @test norm(Tropic.guessHessianLindh(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, h2o2twiceSymbols_SetsOfInternalCoordinatesTest, h2o2Cartesians_SetsOfInternalCoordinatesTest) - expectedPrimitiveHessianGuess_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Creating Hessian Guess Schlegel" begin
            @test norm(Tropic.guessHessianSchlegel(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, h2o2twiceSymbols_SetsOfInternalCoordinatesTest, h2o2Cartesians_SetsOfInternalCoordinatesTest) - expectedPrimitiveHessianGuessSchlegel_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Remove Overwind" begin
            newPrimitiveValues = Tropic.removeOverwind(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, [0; 0; 0; 0; 0; -1.5π])
            @test norm(newPrimitiveValues - valuesFirst_SetsOfInternalCoordinatesTest)  <= 1e-6
            newPrimitiveValues = Tropic.removeOverwind(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, [0; 0; 0; 0; 0;  1.5π])
            @test norm(newPrimitiveValues - valuesSecond_SetsOfInternalCoordinatesTest)  <= 1e-6
        end

        @testset "Get all Values for internal cooridnate system" begin
            @test norm(Tropic.getValuesFromCartesians(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, h2o2Cartesians_SetsOfInternalCoordinatesTest) - expectedValues_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Get Differences for internal coordinate system" begin
            @test norm(Tropic.getDifferences(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, valuesOverwindBeforeFirst_SetsOfInternalCoordinatesTest, valuesOverwindAfterFirst_SetsOfInternalCoordinatesTest) - [0.1000000000;   0.1000000000;   0.1000000000;  -0.1000000000;  -0.1000000000;   0.0628318531]) <= 1e-6
            @test norm(Tropic.getDifferences(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, valuesOverwindBeforeSecond_SetsOfInternalCoordinatesTest, valuesOverwindAfterSecond_SetsOfInternalCoordinatesTest) - [0.1000000000;   0.1000000000;   0.1000000000;  -0.1000000000;  -0.1000000000;  -0.0628318531]) <= 1e-6
        end

        @testset "Add Constraints to the set of internal coordinates" begin
            internalCoordinatesCopy = deepcopy(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest)
            constraints = Tropic.PrimitiveConstraints([Tropic.BondConstraint(1, 4) Tropic.BondConstraint(2, 3)], [Tropic.AngleConstraint(4, 3, 2) Tropic.AngleConstraint(1, 4, 3)], [Tropic.DihedralConstraint(4, 3, 2, 1) Tropic.DihedralConstraint(2, 1, 4, 3)])

            _, listOfConstraints = Tropic.addAndFindConstraints!(internalCoordinatesCopy, constraints, h2o2Cartesians_SetsOfInternalCoordinatesTest)
            
            expectedListOfConstraints = [4, 2, 6, 7, 8, 9]
            @test listOfConstraints == expectedListOfConstraints
            @test length(internalCoordinatesCopy.bonds) == 4
            @test length(internalCoordinatesCopy.angles) == 3
            @test length(internalCoordinatesCopy.dihedrals) == 2
        end

        @testset "Do Primitives have Translation And Rotation Tests" begin
            @test Tropic.hasTranslationAndRotation(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, false) == false
            @test Tropic.hasTranslationAndRotation(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, true) == true
        end

        @testset "toString" begin
            expectedOutput = """Bond(1, 2)   1.8391009799
                                Bond(2, 3)   2.7743366588
                                Bond(3, 4)   1.8719817573
                                Angle(1, 2, 3)   1.7146915548
                                Angle(2, 3, 4)   1.7314341654
                                Dihedral(1, 2, 3, 4)  -1.7098809900
                                """
            @test Tropic.toString(primitiveInternalCoordinateSystem_SetsOfInternalCoordinatesTest, expectedValues_SetsOfInternalCoordinatesTest) == expectedOutput
        end
    end
end