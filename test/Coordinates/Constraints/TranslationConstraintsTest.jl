function translationConstraintTest()
    @testset "TranslationsConstraint Tests" begin
        @testset "Test equality" begin
            translationsX = [Tropic.TranslationX([1, 2, 3, 4, 5]); Tropic.TranslationX([2, 5, 3, 1, 4]); Tropic.TranslationX([1, 2, 3, 4, 5, 6])]
            translationXConstraint = Tropic.TranslationXConstraint([3, 5, 2, 1, 4])

            @test translationsX[1] == translationXConstraint
            @test translationsX[2] == translationXConstraint
            @test translationsX[3] != translationXConstraint
            @test translationXConstraint == translationsX[1]
            @test translationXConstraint == translationsX[2]
            @test translationXConstraint != translationsX[3]

            translationsY = [Tropic.TranslationY([1, 2, 3, 4, 5]); Tropic.TranslationY([2, 5, 3, 1, 4]); Tropic.TranslationY([1, 2, 3, 4, 5, 6])]
            translationYConstraint = Tropic.TranslationYConstraint([3, 5, 2, 1, 4])

            @test translationsY[1] == translationYConstraint
            @test translationsY[2] == translationYConstraint
            @test translationsY[3] != translationYConstraint
            @test translationYConstraint == translationsY[1]
            @test translationYConstraint == translationsY[2]
            @test translationYConstraint != translationsY[3]

            translationsZ = [Tropic.TranslationZ([1, 2, 3, 4, 5]); Tropic.TranslationZ([2, 5, 3, 1, 4]); Tropic.TranslationZ([1, 2, 3, 4, 5, 6])]
            translationZConstraint = Tropic.TranslationZConstraint([3, 5, 2, 1, 4])

            @test translationsZ[1] == translationZConstraint
            @test translationsZ[2] == translationZConstraint
            @test translationsZ[3] != translationZConstraint
            @test translationZConstraint == translationsZ[1]
            @test translationZConstraint == translationsZ[2]
            @test translationZConstraint != translationsZ[3]
        end
        
        @testset "Test Reorder" begin
            @test Tropic.reorderTranslationXConstraint(Tropic.TranslationXConstraint([3, 4, 5, 6, 7]), newOrder_ConstraintTestData).indices == [9, 4, 6, 7, 2]
            @test Tropic.reorderTranslationYConstraint(Tropic.TranslationYConstraint([3, 4, 5, 6, 7]), newOrder_ConstraintTestData).indices == [9, 4, 6, 7, 2]
            @test Tropic.reorderTranslationZConstraint(Tropic.TranslationZConstraint([3, 4, 5, 6, 7]), newOrder_ConstraintTestData).indices == [9, 4, 6, 7, 2]
        end
    end
end