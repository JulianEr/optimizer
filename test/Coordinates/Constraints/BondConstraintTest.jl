function bondConstraintTest()
    @testset "BondConstraint Tests" begin
        @testset "Test equality" begin
            bonds = [Tropic.Bond(1, 2); Tropic.Bond(2, 1); Tropic.Bond(3, 4)]
            hydrogenBonds = [Tropic.HydrogenBond(1, 2); Tropic.HydrogenBond(2, 1); Tropic.HydrogenBond(3, 4)]
            bondConstraint = Tropic.BondConstraint(1, 2)

            @test bonds[1] == bondConstraint
            @test bonds[2] == bondConstraint
            @test bonds[3] != bondConstraint
            @test bondConstraint == bonds[1]
            @test bondConstraint == bonds[2]
            @test bondConstraint != bonds[3]
            @test hydrogenBonds[1] == bondConstraint
            @test hydrogenBonds[2] == bondConstraint
            @test hydrogenBonds[3] != bondConstraint
            @test bondConstraint == hydrogenBonds[1]
            @test bondConstraint == hydrogenBonds[2]
            @test bondConstraint != hydrogenBonds[3]
        end

        @testset "Test Reorder" begin
            @test Tropic.reorderBondConstraint(Tropic.BondConstraint(3, 4), newOrder_ConstraintTestData) == Tropic.BondConstraint(9, 4)
        end
    end
end