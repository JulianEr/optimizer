function angleConstraintTest() 
    @testset "AngleConstraint Tests" begin
        @testset "Test equality" begin
            angles = [Tropic.Angle(1, 2, 3); Tropic.Angle(3, 2, 1); Tropic.Angle(2, 3, 4)]
            angleConstraint = Tropic.AngleConstraint(1, 2, 3)

            @test angles[1] == angleConstraint
            @test angles[2] == angleConstraint
            @test angles[3] != angleConstraint
            @test angleConstraint == angles[1]
            @test angleConstraint == angles[2]
            @test angleConstraint != angles[3]
        end
        
        @testset "Test Reorder" begin
            @test Tropic.reorderAngleConstraint(Tropic.AngleConstraint(3, 4, 5), newOrder_ConstraintTestData) == Tropic.AngleConstraint(9, 4, 6)
        end
    end
end