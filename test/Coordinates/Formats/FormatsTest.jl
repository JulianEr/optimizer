using Test, LinearAlgebra

import Tropic

@info "Start formats tests"

include("FormatsTestData.jl")

include("XYZTest.jl")
include("TranslationRotationZmatrixTest.jl")

xyzFormatTests()
translationRotationZmatrixFormat()