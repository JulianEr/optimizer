function hydrogenBondsTest()
    @testset "HydrogenBond Tests" begin
        @testset "HydrogenBond equality" begin
            @test Tropic.HydrogenBond(1, 2) == Tropic.HydrogenBond(1, 2)
            @test Tropic.HydrogenBond(1, 2) == Tropic.HydrogenBond(2, 1)
            @test Tropic.HydrogenBond(1, 2) == Tropic.Bond(1, 2)
            @test Tropic.HydrogenBond(1, 2) == Tropic.Bond(2, 1)
            @test Tropic.Bond(1, 2) == Tropic.HydrogenBond(1, 2)
            @test Tropic.Bond(2, 1) == Tropic.HydrogenBond(1, 2)
        end

        @testset "HydrogenBond value" begin
            expectedValue = 1.9161483355
            @test isapprox(Tropic.value(Tropic.HydrogenBond(1, 2), h2o2Cartesians_InternalCoordinatesData), expectedValue)
        end

        @testset "HydrogenBond derivative" begin
            @test norm(Tropic.derivativeVector(Tropic.HydrogenBond(1, 2), h2o2Cartesians_InternalCoordinatesData) - expectedDerivativeVectorForBond_InternalCoordinatesData) <= 1e-6
        end

        @testset "HydrogenBond second derivatives" begin
            @test norm(Tropic.secondDerivativeMatrix(Tropic.HydrogenBond(1, 2), h2o2Cartesians_InternalCoordinatesData) - expectedSecondDerivativeMatrixForBond_InternalCoordinatesData) <= 1e-6
        end

        @testset "toString" begin
            @test Tropic.toString(Tropic.HydrogenBond(1, 2)) == "HydrogenBond(1, 2)"
        end

        @testset "Numeric derivatives" begin
            g, H = Tropic.numericGradientsAndHessian(Tropic.HydrogenBond(1, 2), h2o2Cartesians_InternalCoordinatesData)
            @test norm(g - expectedDerivativeVectorForBond_InternalCoordinatesData) <= 1e-6
            @test norm(H - expectedSecondDerivativeMatrixForBond_InternalCoordinatesData) <= 1e-5
        end
    end
end