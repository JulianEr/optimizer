function bondsTest()
    @testset "Bond Tests" begin
        @testset "Bond equality" begin
            @test Tropic.Bond(1, 2) == Tropic.Bond(1, 2)
            @test Tropic.Bond(1, 2) == Tropic.Bond(2, 1)
        end

        @testset "Bond value" begin
            expectedValue = 1.9161483355
            @test isapprox(Tropic.value(Tropic.Bond(1, 2), h2o2Cartesians_InternalCoordinatesData), expectedValue)
        end

        @testset "Bond derivative" begin
            @test norm(Tropic.derivativeVector(Tropic.Bond(1, 2), h2o2Cartesians_InternalCoordinatesData) - expectedDerivativeVectorForBond_InternalCoordinatesData) <= 1e-6
        end

        @testset "Bond second derivatives" begin
            @test norm(Tropic.secondDerivativeMatrix(Tropic.Bond(1, 2), h2o2Cartesians_InternalCoordinatesData) - expectedSecondDerivativeMatrixForBond_InternalCoordinatesData) <= 1e-6
        end

        @testset "toString" begin
            @test Tropic.toString(Tropic.Bond(1, 2)) == "Bond(1, 2)"
        end

        @testset "Numeric derivatives" begin
            g, H = Tropic.numericGradientsAndHessian(Tropic.Bond(1, 2), h2o2Cartesians_InternalCoordinatesData)
            @test norm(g - expectedDerivativeVectorForBond_InternalCoordinatesData) <= 1e-6
            @test norm(H - expectedSecondDerivativeMatrixForBond_InternalCoordinatesData) <= 1e-5
        end

        @testset "Schlegel Hessian Guess Tests" begin
            b = Tropic.Bond(1, 2)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["H", "H"], h2o2Cartesians_InternalCoordinatesData), 0.17202769440365692)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["H", "C"], h2o2Cartesians_InternalCoordinatesData), 0.4531221398312361)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["C", "H"], h2o2Cartesians_InternalCoordinatesData), 0.4531221398312361)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["H", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.8748353258642065)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["H", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.8748353258642065)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["Si", "H"], h2o2Cartesians_InternalCoordinatesData), 0.8748353258642065)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["Bi", "H"], h2o2Cartesians_InternalCoordinatesData), 0.8748353258642065)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["C", "C"], h2o2Cartesians_InternalCoordinatesData), 3.020045421125134)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["C", "Si"], h2o2Cartesians_InternalCoordinatesData), 28.318484161005912)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["C", "Bi"], h2o2Cartesians_InternalCoordinatesData), 28.318484161005912)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["Si", "C"], h2o2Cartesians_InternalCoordinatesData), 28.318484161005912)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["Bi", "C"], h2o2Cartesians_InternalCoordinatesData), 28.318484161005912)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["Si", "Si"], h2o2Cartesians_InternalCoordinatesData), -495.2111593626582)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["Si", "Bi"], h2o2Cartesians_InternalCoordinatesData), -495.2111593626582)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["Bi", "Si"], h2o2Cartesians_InternalCoordinatesData), -495.2111593626582)
            @test isapprox(Tropic.getHessianGuessSchlegel(b, ["Bi", "Bi"], h2o2Cartesians_InternalCoordinatesData), -495.2111593626582)
        end

        @testset "Hessian Guess Tests" begin
            b = Tropic.Bond(1, 2)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["H", "H"], h2o2Cartesians_InternalCoordinatesData), 0.07081870347612924)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["H", "C"], h2o2Cartesians_InternalCoordinatesData), 0.6023460355196609)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["C", "H"], h2o2Cartesians_InternalCoordinatesData), 0.6023460355196609)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["H", "Si"], h2o2Cartesians_InternalCoordinatesData), 1.322181760512694)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["H", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.5)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["Si", "H"], h2o2Cartesians_InternalCoordinatesData), 1.322181760512694)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["Bi", "H"], h2o2Cartesians_InternalCoordinatesData), 0.5)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["C", "C"], h2o2Cartesians_InternalCoordinatesData), 1.6157018746728995)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["C", "Si"], h2o2Cartesians_InternalCoordinatesData), 4.09692814343115)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["C", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.5)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["Si", "C"], h2o2Cartesians_InternalCoordinatesData), 4.09692814343115)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["Bi", "C"], h2o2Cartesians_InternalCoordinatesData), 0.5)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["Si", "Si"], h2o2Cartesians_InternalCoordinatesData), 4.09692814343115)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["Si", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.5)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["Bi", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.5)
            @test isapprox(Tropic.getHessianGuessLindh(b, ["Bi", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.5)
        end
    end
end