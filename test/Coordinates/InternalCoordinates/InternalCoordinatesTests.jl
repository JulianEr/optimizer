using Test, LinearAlgebra

import Tropic

include("InternalCoordinatesData.jl")

@info "Start Internal Coordinates tests"
include("EmptyInternalCoordinateTest.jl")
include("BondsTest.jl")
include("HydrogenBondsTest.jl")
include("AnglesTest.jl")
include("DihedralsTest.jl")
include("TranslationsTest.jl")
include("RotationsTest.jl")

emptyInternalCoodinateTest()
bondsTest()
hydrogenBondsTest()
anglesTest()
dihredralsTest()
translationsTest()
rotationsTest()