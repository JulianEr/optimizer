using Test

import Tropic

include("ConvergenceCheckTestData.jl")
include("ConvergenceCheckOptimizerDouble.jl")
include("GradientsAndChangeCheckTest.jl")
include("GradientsAndValueOrChangeCheckTest.jl")
include("OnlyGradientsCheckTest.jl")
include("ValueAndGradientsCheckTest.jl")
include("ValueGradientsAndChangeCheckTest.jl")