struct ConvergenceCheckOptimizerDouble <: Tropic.AbstractOptimizer
    energy::Float64
    formerEnergy::Float64
    change::Array{Float64}
    gradients::Array{Float64}
    cartesians::Array{Float64}
    formerCartesians::Array{Float64}
end

ConvergenceCheckOptimizerDouble(energy::Float64, formerEnergy::Float64, gradients::Array{Float64}) = ConvergenceCheckOptimizerDouble(energy, formerEnergy, Float64[], gradients, Float64[], Float64[])
ConvergenceCheckOptimizerDouble(change::Array{Float64}, gradients::Array{Float64}) = ConvergenceCheckOptimizerDouble(0.0, 0.0, change, gradients, Float64[], Float64[])
ConvergenceCheckOptimizerDouble(gradients::Array{Float64}) = ConvergenceCheckOptimizerDouble(0.0, 0.0, Float64[], gradients, Float64[], Float64[])
ConvergenceCheckOptimizerDouble(energy::Float64, formerEnergy::Float64, change::Array{Float64}, gradients::Array{Float64}) = ConvergenceCheckOptimizerDouble(energy, formerEnergy, change, gradients, Float64[], Float64[])
ConvergenceCheckOptimizerDouble(energy::Float64, formerEnergy::Float64, gradients::Array{Float64}, cartesians::Array{Float64}, formerCartesians::Array{Float64}) = ConvergenceCheckOptimizerDouble(energy, formerEnergy, Float64[], gradients, cartesians, formerCartesians)

Tropic.getEnergy(self::ConvergenceCheckOptimizerDouble) = self.energy
Tropic.getFormerEnergy(self::ConvergenceCheckOptimizerDouble) = self.formerEnergy
Tropic.getChange(self::ConvergenceCheckOptimizerDouble) = self.change
Tropic.getActiveGradients(self::ConvergenceCheckOptimizerDouble) = self.gradients
Tropic.getCartesians(self::ConvergenceCheckOptimizerDouble) = self.cartesians
Tropic.getFormerCartesians(self::ConvergenceCheckOptimizerDouble) = self.formerCartesians