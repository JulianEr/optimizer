using Test
using Logging
# Really think about these includes...
include("includes.jl")
include("TestUtils/Utils.jl")

logger = SimpleLogger(open("tests_log.dat", "w"), Error)

with_logger(logger) do 
    include("ConvergenceCheck/ConvergenceCheckTest.jl")
    include("Utilities/UtilitiesTest.jl")
    include("BondGraph/BondGraphTest.jl")
    include("Coordinates/CoordinatesTest.jl")
    include("StepControl/StepControlTest.jl")
    include("Hessians/HessiansTest.jl")
    include("Engines/EnginesTest.jl")
    include("CommandLineInterface/CommandLineInterfaceTest.jl")
    include("IntegrationTests/IntegrationTests.jl")
end