const noArguments_CommandLineInterfaceTestData = ["--dry"]

const inputFileName_CommandLineInterfaceTestData = "$(@__DIR__())/h2o2.xyz"
const outputFileName_CommandLineInterfaceTestData = "$(@__DIR__())/h2o2_opt.xyz"
const logFileName_CommandLineInterfaceTestData = "$(@__DIR__())/h2o2.log"

const variableArguments_CommandLineInterfaceTestData = [
    "--dry", 
    "--inputFile", inputFileName_CommandLineInterfaceTestData,
    "--outputFile", outputFileName_CommandLineInterfaceTestData,
    "--coords", "cart",
    "--trustRadius", "0.25",
    "--engine", "Psi4", "path", "somewhere/to/psi/psi4.sh", "memoryInGb", "3", "basisSet", "aug-cc-pvtz", "method", "b3lyp", "charge", "2", "multiplicity", "4", "cores", "4",
    "--hessian", "Calculate",
    "--initialHessian", "Identity", 
    "--updateHessian", "SR1_PBS",
    "--method", "TRM", 
    "--convergenceCheck", "GradientsAndValueOrChangeCheck", "0.42", "0.5", "0.123", "0.432", "0.99",
    "--trustRadiusUpdate", "Bofill",
    "--optimizer", "NonActive",
    "--maxIter", "150",
    "--verbose", 
    "--logFile", logFileName_CommandLineInterfaceTestData
]

const allConstraintsArguments_CommandLineInterfaceTestData = [
    "--dry", 
    "--constraints",
        "b", "2", "6",
        "a", "2", "6", "5",
        "d", "2", "6", "5", "1",
        "tx", "2", "6", "5", "1",
        "ty", "2", "6", "5", "1",
        "tz", "2", "6", "5", "1",
        "ra", "2", "6", "5", "1",
        "rb", "2", "6", "5", "1",
        "rc", "2", "6", "5", "1",
        "t", "3", "4", "7", "8",
        "r", "3", "4", "7", "8",
    "--coords", "ric", "zmat",
    "--engine", "Xtb", "path", "path/to/stb/xtb.bat", "gfnMethod", "3",
    "--convergenceCheck", "Gradients", "0.01", "0.042",
    "--updateHessian", "NBFGS", "5",
    "--trustRadiusUpdate", "Simple", "maxTrustRadius", "1.0", "minTrustRadius", "0.1", "increaseFactor", "1.4142135623730951", "decreaseFactor", "0.5", "rejectThreshold", "-1.0",
    "--trustRadius", "0.25"
]

const ailentArguments_CommandLineInterfaceTestData = [
    "--dry", 
    "--silent",
    "--coords", "zmat",
    "--convergenceCheck", "ValueAndGradients", "0.01", "0.042", "0.123"
]

const h2o2CartesiansFile_CommandLineInterfaceTestData = """
8

H        -2.373300616306734        -1.485702602285717        -0.515493212840031
H         0.000000000000000         0.000000000000000         0.000000000000000
H         0.637720827407217         0.393104306699483        -3.245640975257224
H        -0.795533288526549        -0.556944056242550        -1.726636025582542
O        -1.500977465934547        -1.427513179216086         0.000000000000000
O        -1.013982031462771         0.000000000000000         0.000000000000000
O         0.723077389306323        -0.497818995740499        -2.863399864882036
O        -0.704982591314516        -0.788316796614687        -2.685581053376433
"""

const reorderedH2o2CartesiansFile_CommandLineInterfaceTestData = """
8

H        -2.373300616306734        -1.485702602285717        -0.515493212840031
O        -1.500977465934547        -1.427513179216086         0.000000000000000
O        -1.013982031462771         0.000000000000000         0.000000000000000
H         0.000000000000000         0.000000000000000         0.000000000000000
H         0.637720827407217         0.393104306699483        -3.245640975257224
O         0.723077389306323        -0.497818995740499        -2.863399864882036
O        -0.704982591314516        -0.788316796614687        -2.685581053376433
H        -0.795533288526549        -0.556944056242550        -1.726636025582542
"""