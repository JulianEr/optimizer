function histidinRedBfgs()
    @testset "Mocked Optimization With Convergence check before evaluation of a new step" begin
        bondGraph = Tropic.buildBondGraph(histidinSymbols_IntegrationTests, histidinStartGeometry_IntegrationTests)
        primitives = Tropic.findAllInternalCoordinates(bondGraph, histidinSymbols_IntegrationTests, histidinStartGeometry_IntegrationTests)

        coords = Tropic.RedundantInternalCoordinates(primitives, histidinStartGeometry_IntegrationTests; updateOnCartesianBacktransformation=true)

        gradientsAndHessian = Tropic.AnalyticGradientsUpdatedHessian(
            histidinSymbols_IntegrationTests, 
            Tropic.BfgsUpdate(),
            Psi4EngineDoubleHistidinRed()
        )
        stepControl = Tropic.RSRFO(trustRadius=0.5, useNewtonModelEnergyChange=true)
        trustRadius = Tropic.NonRejectingTrustRadiusUpdate()
        convergenceCheck = Tropic.GradientsAndValueOrChangeCheck(3e-4, Inf, 1.2e-3, Inf, 1e-6)

        o = Tropic.StandardOptimizer(coords, 
            gradientsAndHessian, 
            stepControl, 
            convergenceCheck, 
            trustRadiusUpdate=trustRadius, 
            initialHessian=Tropic.GuessHessian(histidinSymbols_IntegrationTests)
        )

        optimizer = Tropic.StandardOptimizerUseActive(o)

        algorithm = Tropic.OptimizeWithGradientsCheckFromLastCycle(optimizer)

        final = Tropic.optimize!(algorithm)


        @test norm(expectedFinalCartesians_IntegrationTests - final) < 1e-4
    end
end